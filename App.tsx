import React, { useState, useEffect } from 'react';
import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';
import { StatusBar, Platform, Alert, ActivityIndicator, View  } from 'react-native';
import { Provider, useDispatch, useSelector } from 'react-redux';
import {getPersistor, getState, getStore} from './store/store';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { RootSiblingParent } from 'react-native-root-siblings';
import {SocketContext, socket} from './context/socket';
import {toastSuccess, toastError} from './utils/toast';
import {backendMessagesDecoded} from './utils/messagesFormBackend';
import { PersistGate } from 'redux-persist/integration/react';

import {connect} from './store/actions';

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  const [hasConnection, setConnection] = useState(false);
  const [time, setTime] = useState(null);

  const store = getStore();  
  const persistor = getPersistor();

  useEffect(function didMount() {
    socket.on("connect", () => setConnection(true));
    socket.io.on("close", () => setConnection(false));

    socket.on("time-msg", (data) => {
     // setTime(new Date(data.time).toString());
    });

    return function didUnmount() {
      socket.disconnect();
      socket.removeAllListeners();
    };
  }, []);

  if(socket.disconnected){
    //toastError(backendMessagesDecoded('SOCKET_DISCONNECTED'));
    console.log('disconnected');
  }

  if(socket.connected){
   // alert('connected');
  }

  if (!isLoadingComplete) {
    return null;
  } 
  else {

    return (
      <Provider store={store}>
          <PersistGate 
            persistor={persistor}
          >
            <SocketContext.Provider value={socket}>
              <RootSiblingParent>
                <SafeAreaProvider>
                  <Navigation colorScheme={colorScheme} />
                  <StatusBar />
                </SafeAreaProvider>
              </RootSiblingParent>
            </SocketContext.Provider>
          </PersistGate>
      </Provider>
    );
  }
}
