import React, { createContext } from 'react';
import socketIOClient, { io, Socket } from 'socket.io-client';
import {SOCKET_ENDPOINT} from '../config';

const socket = socketIOClient(SOCKET_ENDPOINT, {
    transports: ["websocket"],
  }),
  SocketContext = createContext<Socket>(socket);

export { SocketContext, socket };