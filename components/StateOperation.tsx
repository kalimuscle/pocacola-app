import React from 'react';
import { StyleSheet, View, Text, Pressable  } from 'react-native';
import { AntDesign, FontAwesome } from '@expo/vector-icons';
import { useDispatch, useSelector } from 'react-redux'
import Colors from '../constants/Colors';
import { useNavigation } from '@react-navigation/native';

export default function StateOperation( { state, messageSuccesful, messageFail } ){
  const colorScheme = useSelector(state => state.theme);

  const navigation = useNavigation();

  if(state){
    return (
      <View style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors[colorScheme].background,
        width: '100%'
      }}>
          <FontAwesome name="check" size={64} color="green" />
          <Text category='h5' style={{
              color: Colors[colorScheme].text
            }}>
            {messageSuccesful}
          </Text>
        
      </View>
    );

  }

    return (
      <View style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors[colorScheme].background,
        width: '100%'
      }}>
          <FontAwesome name="exclamation-triangle" size={64} color="red" />
          <Text category='h5' style={{
              color: Colors[colorScheme].text
            }}>
            {messageFail}
          </Text>
        
      </View>
    );
}