import React from 'react';
import { Asset } from 'expo-asset';
import { Platform, StyleSheet, View, Button, Text, ActivityIndicator  } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'
import Colors from '../constants/Colors';

export default function Loading( props: any ){

  const colorScheme = useSelector(state => state.theme);

    return (
        <View style={{
          flex: 1,
          justifyContent: 'center',
          backgroundColor: Colors[colorScheme].background,
        }}>
          <ActivityIndicator size="large" color="#00ff00" />
        </View>
    );
}