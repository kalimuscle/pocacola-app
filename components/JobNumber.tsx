import React from 'react';
import { Asset } from 'expo-asset';
import Colors from '../constants/Colors';
import { useDispatch, useSelector } from 'react-redux'
import { Platform, StyleSheet, View, Button, Text, ActivityIndicator  } from 'react-native';

export default function JobNumber( {position, completed, size, fontSize} ){
  const colorScheme = useSelector(state => state.theme);

   let colorNotification = "orange";

   const positionBeforeYou = position - completed;

   if(position <= 0){
     colorNotification = "red";
   }

   if(position > 0 && positionBeforeYou < 3){
     colorNotification = "green"
   }

    return (
      <View
      style={{
         width: size,
         height:  size,
         backgroundColor: "none",
         borderRadius: size/2,
         //flex: 1,
         alignItems: 'center'
       }}
     >
      <Text  style={{
        fontSize: fontSize,
        color: Colors[colorScheme].text
      }}># {position}</Text>
     </View>
    );
}
