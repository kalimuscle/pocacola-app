import React from 'react';
import { Asset } from 'expo-asset';
import { Share, StyleSheet, View, Text, Pressable  } from 'react-native';
import AppLoading from 'expo-app-loading';
import { PropsService } from '@ui-kitten/components/devsupport';
import * as Sharing from 'expo-sharing';
import Colors from '../constants/Colors';
import { useDispatch, useSelector } from 'react-redux';

export default function ShareLinkButton({titleWindow, buttonText, buttonType, url, message}){
  const colorScheme = useSelector(state => state.theme);

  const onShare = async () => {
    try {
      await Share.share({
        url,
        message
      });

    } catch (error) {
      //Dalert(error.message);
    }
  };

    return (
      
      <View 
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'transparent'
        }}
      >
        <Text style={{color: Colors[colorScheme].text, fontSize: 18}}>{titleWindow}</Text>
        
        <Pressable
          onPress={() => onShare()}
          style={({ pressed }) => ({
            opacity: pressed ? 0.5 : 1,
            margin: 8,
            width: '100%',
            maxWidth: 300,
            borderRadius: 30,
            padding: 16,
            backgroundColor: Colors[colorScheme][buttonType]
        })}>
            <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}> {buttonText} </Text>
        </Pressable>
      </View>
    );
}
