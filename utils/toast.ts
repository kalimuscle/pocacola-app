import Toast from 'react-native-root-toast';

  export async function toastSuccess(message: string) {
    return Toast.show(message, {
        duration: Toast.durations.LONG,
        position: Toast.positions.CENTER,
        shadow: false,
        opacity: 0.9,
        animation: true,
        hideOnPress: true,
        backgroundColor: "#3366ff",
        textColor: "#fff",
        delay: 0,
    });
  }

  export async function toastError(message: string) {
    return Toast.show(message, {
        duration: 5000,
        position: Toast.positions.CENTER,
        shadow: false,
        opacity: 0.9,
        animation: true,
        hideOnPress: true,
        backgroundColor: "#cf0e0e",
        textColor: "#fff",
        delay: 0,
    });
  }