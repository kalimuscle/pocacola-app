import { Linking } from 'react-native';

export async function connected(): Promise<Boolean> {
    return await Linking.canOpenURL('https://google.com')
}