import I18n from '../i18n/locales';

export const backendMessagesDecoded = (code: string) : string => {

    switch(code){
        case 'SOCKET_DISCONNECTED':
            return I18n.t('messages.SOCKET_DISCONNECTED'); 
            break;
            
        case 'TICKET_NOT_EXIST': 
            return I18n.t('messages.TICKET_NOT_EXIST'); 
            break;

        case 'QUEUE_NOT_ACTIVATED': 
            return I18n.t('messages.QUEUE_NOT_ACTIVATED'); 
            break;

        case 'QUEUE_NOT_EXIST': 
            return I18n.t('messages.QUEUE_NOT_EXIST'); 
            break;

        case 'QUEUE_USER_AND_MANAGER_EQUAL': 
            return I18n.t('messages.QUEUE_USER_AND_MANAGER_EQUAL'); 
            break;

        case 'TICKET_EXIST': 
            return I18n.t('messages.TICKET_EXIST'); 
            break;

        case 'USER_ALREADY_HAVE_TICKET': 
            return I18n.t('messages.USER_ALREADY_HAVE_TICKET'); 
            break;

        case 'TICKET_EXPIRED': 
            return I18n.t('messages.TICKET_EXPIRED'); 
            break;

        case 'TICKET_CORRUPTED': 
            return I18n.t('messages.TICKET_CORRUPTED'); 
            break;

        case 'TICKET_NOT_AUTHORIZED_VEFIFIER': 
            return I18n.t('messages.TICKET_NOT_AUTHORIZED_VEFIFIER'); 
            break;

        case 'QUEUE_FINALIZED': 
            return I18n.t('messages.QUEUE_FINALIZED'); 
            break;

        case 'CREATED_QUEUE': 
            return I18n.t('messages.CREATED_QUEUE'); 
            break;

        case 'JOINED_QUEUE': 
            return I18n.t('messages.JOINED_QUEUE'); 
            break;

        case 'FINALIZED_QUEUE': 
            return I18n.t('messages.FINALIZED_QUEUE'); 
            break;

        case 'PAUSED_QUEUE': 
            return I18n.t('messages.PAUSED_QUEUE'); 
            break;

        case 'RESUME_QUEUE': 
            return I18n.t('messages.RESUME_QUEUE'); 
            break;

        case 'GIVE_UP_TICKET': 
            return I18n.t('messages.GIVE_UP_TICKET'); 
            break;

        case 'VERIFIED_TICKET': 
            return I18n.t('messages.VERIFIED_TICKET'); 
            break;

        case 'NOT_CONNECTION':
            return I18n.t('messages.NOT_CONNECTION'); 
            break;


            
        default: return '';
    }
}