/**
 * Learn more about deep linking with React Navigation
 * https://reactnavigation.org/docs/deep-linking
 * https://reactnavigation.org/docs/configuring-links
 */

import { LinkingOptions } from '@react-navigation/native';
import * as Linking from 'expo-linking';

import { RootStackParamList } from '../types';

const prefix = Linking.createURL('/');

const linking: LinkingOptions<RootStackParamList> = {
  prefixes: [prefix],
  config: {
    screens: {
      QeueScreen: {
        screens: {
          Home: 'Home',
          Queues: 'Queues',
          Tickets: 'Tickets',
          Config: 'Config',
          Info: 'Info',
        }
      },
      SplashScreen: 'splash',
      CreateQeueScreen: 'createQeue',
      QRCodeQeueScreen: 'qrQeue',
      ProcessScanScreen: 'processItem',
      QRScanQeueScreen: 'qrScan',
      JoinQeueScreen: 'joinQeue',
      JoinStateQeueScreen: 'joinState',
      ListQeueScreen: 'list',
      ListTicketScreen: 'ticket',
      DetailQeueScreen: 'get',
      DetailTicketScreen: 'getTicket',
      ConfigurationScreen: 'getConfig',
      DocumentationScreen: 'info',
      //CheckLinkigAppScreen: 'linkingAppCheck',
      Modal: 'modal',
      NotFound: '*',
    },
  },
};

export default linking;
