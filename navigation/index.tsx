/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */

import { NavigationContainer, DefaultTheme, DarkTheme, useNavigation } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { ColorSchemeName, Pressable, Platform, StyleSheet, View  } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { BlurView } from 'expo-blur';
import * as Linking from 'expo-linking';

import Colors from '../constants/Colors';
import { useDispatch, useSelector } from 'react-redux';
import ModalScreen from '../screens/ModalScreen';
import NotFoundScreen from '../screens/NotFoundScreen';

import QeueScreen from '../screens/QeueScreen';
import CreateQeueScreen from '../screens/CreateQeueScreen';
import QRCodeQeueScreen from '../screens/QRCodeQeueScreen';
import QRScanQeueScreen from '../screens/QRScanQeueScreen'; 
import JoinQeueScreen from '../screens/JoinQeueScreen';
import JoinStateQeueScreen from '../screens/JoinStateQeueScreen';
import ListQeueScreen from '../screens/ListQeueScreen';
import ListTicketScreen from '../screens/ListTicketScreen';
import DetailQeueScreen from '../screens/DetailQeueScreen';
import DetailTicketScreen from '../screens/DetailTicketScreen';
import DocumentationScreen from '../screens/DocumentationScreen';
import ConfigurationScreen from '../screens/ConfigurationScreen';
import ProcessScanScreen from '../screens/ProcessScanScreen';
import CheckLinkigAppScreen from '../screens/CheckLinkigAppScreen';
import SplashScreen from '../screens/SplashScreen';

import I18n from '../i18n/locales';

import { RootStackParamList, RootTabParamList, RootTabScreenProps } from '../types';
import LinkingConfiguration from './LinkingConfiguration';



export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      <RootNavigator />
    </NavigationContainer>
  );
}

/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */
const Stack = createNativeStackNavigator<RootStackParamList>();

function RootNavigator() {
  const lang = useSelector(state => state.lang);

  I18n.locale = lang; 
  const colorScheme = useSelector(state => state.theme);
  const navigation = useNavigation ();

  const buttonHeaderLeft = (redirect: string) => {

    let func;

    switch(redirect){
      case 'home': 
        func = () => navigation.navigate('QeueScreen');
        break;
      case 'scan': 
        func = () => navigation.navigate('QRScanQeueScreen');
      
      case 'back':
      default: 
        func = () => navigation.goBack();
    }

    return (
      <Pressable
      onPress={func}
      style={({ pressed }) => ({
              opacity: pressed ? 0.5 : 1,
              marginRight: 8 
    })}>
        <FontAwesome
          name="arrow-left"
          size={24}
          color={Colors[colorScheme].text}
          
        />
    </Pressable>
    );

  }
  const buttonGoHome = () => (
    <Pressable
      onPress={() => navigation.navigate('QeueScreen')}
      style={({ pressed }) => ({
              opacity: pressed ? 0.5 : 1,
              marginRight: 8 
    })}>
        <FontAwesome
          name="arrow-left"
          size={24}
          color={Colors[colorScheme].text}
          
        />
    </Pressable>
  );
  
  const buttonHeaderRight = () => (
    <Pressable
      onPress={() => navigation.navigate('QRScanQeueScreen')}
      style={({ pressed }) => ({
        opacity: pressed ? 0.5 : 1,
      })}>
      <FontAwesome
        name="qrcode"
        size={24}
        color={Colors[colorScheme].text}
        style={{ marginRight: 15 }}
      />
    </Pressable>
  );

  const headerBackgroundStyle = () => <BlurView tint="dark" intensity={100} style={{backgroundColor: Colors[colorScheme].background}} />

  return (
    <Stack.Navigator screenOptions={{ headerStyle: {
        backgroundColor: Colors[colorScheme].background, 
      }
    }}
    initialRouteName="splash"
    >
      <Stack.Screen 
        name="SplashScreen" 
        component={SplashScreen}
        options={({ navigation }) => ({
          headerShown: false,
         
        })}
      />
      <Stack.Screen 
        name="QeueScreen" 
        component={BottomTabNavigator}
        options={({ navigation }) => ({
          headerShown: false,
          
          
        })}
      />
      <Stack.Screen name="NotFound" component={NotFoundScreen} options={{ title: 'Oops!' }} />

      <Stack.Group >
        <Stack.Screen 
          name="CreateQeueScreen" 
          component={CreateQeueScreen}
          options={({ navigation }: NativeStackScreenProps<'CreateQeueScreen'>) => ({
            title: I18n.t('createQueue.title'),
            //headerShown: false,
            headerTitleStyle:{
              color: Colors[colorScheme].text,
            },
            headerRight: () => (
              buttonHeaderRight()
            ),
            headerLeft: () => (
              buttonHeaderLeft('back')
            ),
            
          })}
        />
        <Stack.Screen 
          name="JoinQeueScreen" 
          component={JoinQeueScreen} 
          options={({ navigation }: NativeStackScreenProps<'JoinQeueScreen'>) => ({
            title: I18n.t('joinQueue.title'),
            headerTitleStyle:{
              color: Colors[colorScheme].text,
            },
            //headerShown: false,
            headerRight: () => (
              buttonHeaderRight()
            ),
            headerLeft: () => (
              buttonHeaderLeft('home')
            ),
            
          })}
        />
        <Stack.Screen 
          name="QRCodeQeueScreen" 
          component={QRCodeQeueScreen}
          options={({ navigation }: NativeStackScreenProps<'QRCodeQeueScreen'>) => ({
            title: I18n.t('qrCode.title'),
            headerTitleStyle:{
              color: Colors[colorScheme].text,
            },
            //headerShown: false,
            headerRight: () => (
              buttonHeaderRight()
            ),
            headerLeft: () => (
              buttonHeaderLeft('back')
            ),
            
          })}
        />
        <Stack.Screen 
          name="QRScanQeueScreen" 
          component={QRScanQeueScreen}
          options={({ navigation }: NativeStackScreenProps<'QRScanQeueScreen'>) => ({
            title: I18n.t('qrScan.title'),
            headerTitleStyle:{
              color: Colors[colorScheme].text,
            },
            //headerShown: false,
            headerRight: () => (
              buttonHeaderRight()
            ),
            headerLeft: () => (
              buttonHeaderLeft('back')
            ),
            
          })}
        />
        <Stack.Screen 
          name="DetailQeueScreen" 
          component={DetailQeueScreen}
          options={({ navigation }: NativeStackScreenProps<'DetailQeueScreen'>) => ({
            title: I18n.t('detailQueue.title'),
            headerTitleStyle:{
              color: Colors[colorScheme].text,
            },
            //headerShown: false,
            headerRight: () => (
              buttonHeaderRight()
            ),
            headerLeft: () => (
              buttonHeaderLeft('back')
            ),
            
          })}
        />
        <Stack.Screen 
          name="DetailTicketScreen" 
          component={DetailTicketScreen}
          options={({ navigation }: NativeStackScreenProps<'DetailTicketScreen'>) => ({
            title: I18n.t('detailTicket.title'),
            headerTitleStyle:{
              color: Colors[colorScheme].text,
            },
            //headerShown: false,
            headerRight: () => (
              buttonHeaderRight()
            ),
            headerLeft: () => (
              buttonHeaderLeft('back')
            ),
            
          })}
        />
        <Stack.Screen 
          name="ProcessScanScreen" 
          component={ProcessScanScreen}
          options={({ navigation }: NativeStackScreenProps<'ProcessScanScreen'>) => ({
            title: I18n.t('processScan.title'),
            headerTitleStyle:{
              color: Colors[colorScheme].text,
            },
            //headerShown: false,
            headerRight: () => (
              buttonHeaderRight()
            ),
            headerLeft: () => (
              buttonHeaderLeft('home')
            ),
            
          })}
        />
        {/* <Stack.Screen 
          name="CheckLinkigAppScreen" 
          component={CheckLinkigAppScreen}
          options={({ navigation }: NativeStackScreenProps<'CheckLinkigAppScreen'>) => ({
            
            headerTitleStyle:{
              color: Colors[colorScheme].text,
            },
            headerShown: false,
            headerRight: () => (
              buttonHeaderRight()
            ),
            headerLeft: () => (
              buttonHeaderLeft('home')
            ),
            
          })}
        /> */}
      </Stack.Group>
      <Stack.Group screenOptions={{presentation: 'modal'}}>
        
        {/*
        
        <Stack.Screen name="JoinStateQeueScreen" component={JoinStateQeueScreen} options={{ headerShown: true, headerBackVisible: true }} />
        
        
        <Stack.Screen name="Modal" component={ModalScreen} /> */}
      </Stack.Group>
    </Stack.Navigator>
  );
}

/**
 * A bottom tab navigator displays tab buttons on the bottom of the display to switch screens.
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */
 const BottomTab = createBottomTabNavigator<RootTabParamList>();

 function BottomTabNavigator() {
  const colorScheme = useSelector(state => state.theme);
   const navigation = useNavigation ();
   const lang = useSelector(state => state.lang);

  I18n.locale = lang; 

   const scanOption =  () => (
    <Pressable
      onPress={() => navigation.navigate('QRScanQeueScreen')}
      style={({ pressed }) => ({
        opacity: pressed ? 0.5 : 1,
      })}>
      <FontAwesome
        name="qrcode"
        size={25}
        color={Colors[colorScheme].text}
        style={{ marginRight: 15 }}
      />
    </Pressable>
  )
 
   return (
      <BottomTab.Navigator
        //initialRouteName="Home"
        screenOptions={{
          tabBarActiveTintColor: Colors[colorScheme].tint,
          tabBarStyle: {backgroundColor: Colors[colorScheme].background,},
          headerStyle: {backgroundColor: Colors[colorScheme].background,},
          tabBarBackground: () => (
            <BlurView tint="dark" intensity={100} style={{backgroundColor: Colors[colorScheme].background}} />
          ),
          // headerBackground: () => (
          //     <BlurView tint="dark" intensity={100} style={{backgroundColor: Colors[colorScheme].background}} />
          // ),
        }}
      >
          <BottomTab.Screen
            name="Home"
            component={QeueScreen}
            options={({ navigation }: RootTabScreenProps<'Home'>) => ({
              title: I18n.t('homepage.title'),
              headerShown: false,
              tabBarIcon: ({ color }) => <TabBarIcon name="home" color={color} />,
              headerRight: () => (
                scanOption()
              ),
            })}
          />
          <BottomTab.Screen
            name="Queues"
            component={ListQeueScreen}
            options={({ navigation }: RootTabScreenProps<'Queues'>) => ({
              title: I18n.t('listQueue.title'),
              headerTitleStyle:{
                color: Colors[colorScheme].text,
              },
              tabBarIcon: ({ color }) => <TabBarIcon name="list" color={color} />,
              headerRight: () => (
                scanOption()
              ),
            })}
          />
          <BottomTab.Screen
            name="Tickets"
            component={ListTicketScreen}
            options={({ navigation }: RootTabScreenProps<'Tickets'>) => ({
              title: I18n.t('listTicket.title'),
              headerTitleStyle:{
                color: Colors[colorScheme].text,
              },
              tabBarIcon: ({ color }) => <TabBarIcon name="ticket" color={color} />,
              headerRight: () => (
                scanOption()
              ),
            })}
          />
          <BottomTab.Screen
            name="Config"
            component={ConfigurationScreen}
            options={({ navigation }: RootTabScreenProps<'Config'>) => ({
              title: I18n.t('configuration.title'),
              headerTitleStyle:{
                color: Colors[colorScheme].text,
              },
              tabBarIcon: ({ color }) => <TabBarIcon name="cog" color={color} />,
              headerRight: () => (
                scanOption()
              ),
            })}
          />
          <BottomTab.Screen
            name="Info"
            component={DocumentationScreen}
            options={({ navigation }: RootTabScreenProps<'Info'>) => ({
              title: I18n.t('documentation.title'),
              headerTitleStyle:{
                color: Colors[colorScheme].text,
              },
              tabBarIcon: ({ color }) => <TabBarIcon name="info" color={color} />,
              headerRight: () => (
                scanOption()
              ),
            })}
          />
     </BottomTab.Navigator>
   );
 }
 
 /**
  * You can explore the built-in icon families and icons on the web at https://icons.expo.fyi/
  */
 function TabBarIcon(props: {
   name: React.ComponentProps<typeof FontAwesome>['name'];
   color: string;
 }) {
   return <FontAwesome size={24} style={{ marginBottom: -3 }} {...props} />;
 }