import { StatusBar } from 'expo-status-bar';
import {useState} from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { Platform, StyleSheet, View, Text  } from 'react-native';
//import { Button, Icon, Layout, Spinner, Text, Input} from '@ui-kitten/components';
import { AntDesign } from '@expo/vector-icons';
import I18n from '../i18n/locales';

export default function JoinStateQeueScreen() {
  const [status, setStatus] = useState(true);
  const lang = useSelector(state => state.lang);

  I18n.locale = lang; 

  const iconComponent = status ? (
    <AntDesign name="checkcircleo" size={128} color="green" />
  ) :
  (
    <AntDesign name="exclamationcircleo" size={128} color="red" />
  );

  return (
    <View>
      <Text>join state qeue</Text>
    </View>
  );
}
