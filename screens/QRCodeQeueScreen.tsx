import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { Platform, StyleSheet, View, Text, Pressable, TouchableOpacity  } from 'react-native';
import * as Linking from 'expo-linking';

// import { BarCodeScanner } from 'expo-barcode-scanner';
import QRCode from 'react-native-qrcode-svg';
import Loading from '../components/Loading';
import ShareLinkButton from '../components/ShareLinkButton';
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import {FontAwesome } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import I18n from '../i18n/locales';
import {LINKING_URL} from '../config';

const genRandomHex = (size: number) => [...Array(size)].map(() => Math.floor(Math.random() * 16).toString(16)).join('');

export default function QRCodeQeueScreen({route}) {

  const sub = useSelector(state => state.sub);
  const [loading, setLoading] = useState(false);
  const colorScheme = useSelector(state => state.theme);
  const navigation = useNavigation();

  const lang = useSelector(state => state.lang);

  I18n.locale = lang; 

  const { data, qrType } = route.params;

  const qrStringGenerated = (data: any, qrType: string) : string => {
    let arrayEle = new Array();
    arrayEle.push(qrType);

    const random1 = genRandomHex(40);
    const random2 = genRandomHex(60);

    switch(qrType){
      case 'queue':
        arrayEle.push(data.queue.id);
        break;
      case 'ticket':
        arrayEle.push(data.id);
        arrayEle.push(random1);
        arrayEle.push(data.owner);
        break;
    }

    const result = arrayEle.toString();

    return result
  }


  const generateLinking = () : string => {
    const linking = Linking.createURL('processItem', {
      queryParams: { data: qrStringGenerated(data, qrType) },
    });

    const url = LINKING_URL + linking;

    return url
  } 

  if(loading){
    return (
      <Loading/>
    );
  }

  let component;

  if(qrType === 'queue'){
    component = <Text style={{
      color: Colors[colorScheme].text, 
      fontSize: 24,
      paddingTop: 24,
      paddingBottom: 16
    }}>
      {I18n.t('qrCode.labelQueue')}
    </Text>
  }

  if(qrType === 'ticket'){
    component = <Text style={{
      color: Colors[colorScheme].text, 
      fontSize: 24,
      paddingTop: 24,
      paddingBottom: 16
    }}>{I18n.t('qrCode.labelTicket')}</Text>
  }
  
  return (
      <View 
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: Colors[colorScheme].background,
          width: '100%'
        }}
      >
        
        <View style={{
          width: '100%',
          height: '80%',
          alignItems: 'center',
          justifyContent: 'center',
          
        }}>
            <View style={{
                //flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                width: '100%',
                height: '15%'
              }}>

              </View>
            
            <View style={{
               // flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                height: '70%'
              }}>
                  {component}
                <View style={{
                    backgroundColor: '#fff',
                    // width: '280',
                    height: 'auto',
                    //maxHeight: 260,
                    borderRadius: 4,
                    padding: 8
                  }}>
                  <QRCode 
                    value={qrStringGenerated(data, qrType)}
                    size={250}
                  />
                </View>
            </View>
            <View style={{
                //flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                width: '100%',
                height: '15%'
              }}>
            </View>
          </View>
          <ShareLinkButton
            titleWindow="" 
            buttonText={qrType == 'queue' ? I18n.t('qrCode.shareQueue') : I18n.t('qrCode.shareTicket')}
            buttonType="buttonSuccess"
            url={generateLinking()}
            message={generateLinking()}
          />
      </View>
    );
}
