import { StatusBar } from 'expo-status-bar';
import {useState, useEffect, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { Platform, StyleSheet, View, Text, Pressable  } from 'react-native';
import * as Linking from 'expo-linking';
import {toastSuccess, toastError} from '../utils/toast';
import {backendMessagesDecoded} from '../utils/messagesFormBackend';
import Loading from '../components/Loading';
import JobNumber from '../components/JobNumber';
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import {FontAwesome } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import {SocketContext} from '../context/socket';
import {removeTicket} from '../store/actions';
import I18n from '../i18n/locales';
import ShareLinkButton from '../components/ShareLinkButton';
import {LINKING_URL} from '../config';

const genRandomHex = (size: number) => [...Array(size)].map(() => Math.floor(Math.random() * 16).toString(16)).join('');

export default function DetailTicketScreen({route}) {
  const socket = useContext(SocketContext);
  const sub = useSelector(state => state.sub);
  const [pending, setPending] = useState(false);
  const colorScheme = useSelector(state => state.theme);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const {res:{ payload}} = route.params;
  const [item, setItem] = useState(payload);
  const lang = useSelector(state => state.lang);

  I18n.locale = lang;

  useEffect(() => {
    const ticketGiveUp = (data: any) => {

      const res = JSON.parse(data);
      if(!res.ok){
        toastError(backendMessagesDecoded(res.msg));
      }
      else{
        dispatch(removeTicket(res.payload))
        setPending(false);
        toastSuccess(backendMessagesDecoded(res.action));
        navigation.goBack();
      }
    };
  
    socket.on('GAVE_UP_TICKET', ticketGiveUp);

    return () => {
      socket.off('GAVE_UP_TICKET', ticketGiveUp)
    }
  }, [socket]);

  const giveUpTicket = async (id: string) => {
    const value ={
      sub,
      ticket: id
    }

    socket.send(JSON.stringify({
      type: 'GIVE_UP_TICKET',
      content: value
    }));

    setPending(true);
  }

  if(pending){
    return <Loading />
  }

  const donateLinkGenerated = () : string => {
    let arrayEle = new Array();
    arrayEle.push('donate');

    const random1 = genRandomHex(40);
    const random2 = genRandomHex(60);

    arrayEle.push(item.id);
    arrayEle.push(random1);
    arrayEle.push(item.owner);
    
    const result = arrayEle.toString();

    return result
  }


  const generateLinking = () : string => {
    const linking = Linking.createURL('processItem', {
      queryParams: { data: donateLinkGenerated() },
    });

    const url = LINKING_URL + linking;

    return url
  } 

  const meanTime = ( createdAt: number, elapsedTime: number, completed: number) : string => {

    if(completed == 0)
    return I18n.t('general.calculating');

    const absDiffMilliSeconds = Math.abs(elapsedTime - createdAt) / 1000

    let meanMilliSeconds = absDiffMilliSeconds / completed;

    // calculate days
    const days = Math.floor(meanMilliSeconds / 86400);
    meanMilliSeconds -= days * 86400;

    // calculate hours
    const hours = Math.floor(meanMilliSeconds / 3600) % 24;
    meanMilliSeconds -= hours * 3600;

    // calculate minutes
    const minutes = Math.floor(meanMilliSeconds / 60) % 60;
    meanMilliSeconds -= minutes * 60;

    let difference = '';
    if (days > 0) {
      difference += (days === 1) ? `${days} ${I18n.t('general.day')}, ` : `${days} ${I18n.t('general.days')} `;
    }

    if(hours === 0 && minutes === 0){
      return I18n.t('general.leastOneMin')
    }

    difference += (hours === 0 || hours === 1) ? `${hours} ${I18n.t('general.hour')} ` : `${hours} ${I18n.t('general.hours')} `;

    difference += (minutes === 0 || hours === 1) ? `${minutes} ${I18n.t('general.minutes')}` : `${minutes} ${I18n.t('general.minutes')}`;

    return difference;

  }

  const durationQueue = ( time: number, maxTime: number) : string => {

    const totalHours = (maxTime *60*60*1000);
    const finalTime = time + totalHours;

    const dateNow = Date.now();
    let diffInMilliSeconds = Math.abs(finalTime - dateNow) / 1000;

    // calculate days
    const days = Math.floor(diffInMilliSeconds / 86400);
    diffInMilliSeconds -= days * 86400;

    // calculate hours
    const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
    diffInMilliSeconds -= hours * 3600;

    // calculate minutes
    const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
    diffInMilliSeconds -= minutes * 60;

    let difference = '';
    if (days > 0) {
      difference += (days === 1) ? `${days} ${I18n.t('general.day')}, ` : `${days} ${I18n.t('general.days')} `;
    }

    difference += (hours === 0 || hours === 1) ? `${hours} ${I18n.t('general.hour')} ` : `${hours} ${I18n.t('general.hours')} `;

    difference += (minutes === 0 || hours === 1) ? `${minutes} ${I18n.t('general.minutes')}` : `${minutes} ${I18n.t('general.minutes')}`; 

    return difference;
  }

  return (
    <View style={{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: Colors[colorScheme].background,
      width: '100%'
    }}
    >
      <View style={{
          // flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          // backgroundColor: 'rgb(18, 18, 18)',

          width: '100%',
          height: '10%'
        }}
      >
        <Text numberOfLines={1} ellipsizeMode='tail' style={{color: Colors[colorScheme].text, fontSize: 20, padding: 8,}}>{item.name || item.id}</Text>
        
      </View>
      <View style={{
        width: '100%',
        height: '70%',
        alignItems: 'center',
        justifyContent: 'center',
      }} >
          <JobNumber position={item.queuePosition} completed={item.completed} size={60} fontSize={40}/>
          <Text style={{color: Colors[colorScheme].text, fontSize: 16, padding: 24, paddingBottom: 0}}>{item.queue.description}</Text>
          
          <View style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: Colors[colorScheme].background,
                width: '100%'
              }}
            >
              <View style={{
                flex: 1,
                alignItems: 'flex-start',
                justifyContent: 'center',
                backgroundColor: Colors[colorScheme].background,
                width: '80%',
                marginLeft: 24
              }}>
                <Text  style={{color: Colors[colorScheme].text, margin: 4}}>{I18n.t('detailTicket.state')} </Text>
                <Text  style={{color: Colors[colorScheme].text, margin: 4}}>{I18n.t('detailTicket.progress')} </Text>
                <Text  style={{color: Colors[colorScheme].text, margin: 4}}>{I18n.t('detailTicket.timeService')} </Text>
                <Text  style={{color: Colors[colorScheme].text, margin: 4}}>{I18n.t('detailTicket.waitingTime')} </Text>

              </View>

              <View style={{
                flex: 1,
                alignItems: 'flex-end',
                justifyContent: 'center',
                backgroundColor: Colors[colorScheme].background,
                width: '80%',
                marginRight: 24
              }}>
                <Text  style={{color: Colors[colorScheme].text, margin: 4}}> {item.queue.state} </Text>
                <Text style={{color: Colors[colorScheme].text, margin: 4}}>{item.completed} / {item.queue.availability}</Text>
                <Text style={{color: Colors[colorScheme].text, margin: 4}}>{durationQueue(item.queue.createdAt, item.queue.maxTime)} </Text>
                <Text style={{color: Colors[colorScheme].text, margin: 4}}>{meanTime(item.queue.createdAt, item.queue.elapsedTime, item.completed)}</Text>
              </View>

          </View>
          <View style={{width: '100%', alignItems: 'center', justifyContent: 'center',}}>
            <Pressable
              onPress={()=>{}}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
                margin: 8,
                width: '85%',
                borderRadius: 20,
                padding: 8,
                backgroundColor: Colors[colorScheme].buttonSuccess
              })}>
                <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}> {I18n.t('detailTicket.donate')}</Text>
            </Pressable>

            {/* <ShareLinkButton 
              titleWindow="" 
              buttonText="DONANDO SU TICKET AQUI" 
              buttonType="buttonSuccess"
              url={generateLinking()}
              message={generateLinking()}
            /> */}
          </View>
      </View>

      <View style={{
                // flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                // backgroundColor: 'rgb(18, 18, 18)',
                width: '100%',
                height: '20%',
                flex: 1,
                flexDirection: 'row',
              }}
              >
                <Pressable
                  onPress={()=>{
                    navigation.navigate('QRCodeQeueScreen', {
                      data: item,
                      qrType: 'ticket'
                    });
                  }}
                  style={({ pressed }) => ({
                    opacity: pressed ? 0.5 : 1,
                    margin: 8,
                    width: '45%',
                    borderRadius: 30,
                    padding: 16,
                    backgroundColor: Colors[colorScheme].buttonSuccess
                  })}>
                    <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}> {I18n.t('detailTicket.consume')} </Text>
                </Pressable>
      
                <Pressable
                  onPress={()=> giveUpTicket(item.id)}
                  style={({ pressed }) => ({
                    opacity: pressed ? 0.5 : 1,
                    margin: 8,
                    width: '45%',
                    borderRadius: 30,
                    padding: 16,
                    backgroundColor: Colors[colorScheme].buttonDanger
                  })}>
                    <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}> {I18n.t('detailTicket.abandom')} </Text>
                </Pressable>
              </View>
    </View>
  );
}
