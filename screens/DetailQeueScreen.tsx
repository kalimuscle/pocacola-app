import { StatusBar } from 'expo-status-bar';
import {useState, useEffect, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { Platform, StyleSheet, View, Text, Pressable  } from 'react-native';
import {toastSuccess, toastError} from '../utils/toast';
import {backendMessagesDecoded} from '../utils/messagesFormBackend';
import Loading from '../components/Loading';
import JobNumber from '../components/JobNumber';
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import {FontAwesome } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import {SocketContext} from '../context/socket';
import {removeQueue, resumeQueue, pauseQueue} from '../store/actions';
import I18n from '../i18n/locales';
import ShareLinkButton from '../components/ShareLinkButton';
import * as Linking from 'expo-linking';
import {LINKING_URL} from '../config';

export default function DetailQeueScreen({route}) {
  const socket = useContext(SocketContext);
  const sub = useSelector(state => state.sub);
  const [pending, setPending] = useState(false);
  const colorScheme = useSelector(state => state.theme);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const lang = useSelector(state => state.lang);

  I18n.locale = lang; 

  const {res:{ payload}} = route.params;
  const [item, setItem] = useState(payload);

  useEffect(() => {
    const queueFinalize = (data: any) => {
      const res = JSON.parse(data);
      if(!res.ok){
        toastError(backendMessagesDecoded(res.msg));
      }
      else{
        dispatch(removeQueue(res.payload));
        toastSuccess(backendMessagesDecoded(res.action));
        setPending(false);
        navigation.goBack();
      }
    };
  
    const queuePause = (data: any) => {
      const res = JSON.parse(data);
      if(!res.ok){
        toastError(backendMessagesDecoded(res.msg));
      }
      else{
        dispatch(pauseQueue(res.payload))
        setItem({
          ...item,
          state: 'paused'
        });
        toastSuccess(backendMessagesDecoded(res.action));
        setPending(false);
      }
    };
  
    const queueResume = (data: any) => {
      const res = JSON.parse(data);
      if(!res.ok){
        toastError(backendMessagesDecoded(res.msg));
      }
      else{
        dispatch(resumeQueue(res.payload))
        setItem({
          ...item,
          state: 'resume'
        });
        toastSuccess(backendMessagesDecoded(res.action));
        setPending(false);
      }
    };

    socket.on('NOTIFICATION_FINALIZED_QUEUE', () => {
      alert('NOTIFICATION_FINALIZED_QUEUE');
    });
  
    socket.on('FINALIZED_QUEUE', queueFinalize);
    socket.on('PAUSED_QUEUE', queuePause);
    socket.on('RESUMED_QUEUE', queueResume);

    return () => {
      socket.off('FINALIZED_QUEUE', queueFinalize),
      socket.off('PAUSED_QUEUE', queuePause),
      socket.off('RESUMED_QUEUE', queueResume)
    }
  }, [socket]);

  const finalizeQueue = async (id: string) => {
    const value ={
      sub,
      queue: id
    }

    socket.send(JSON.stringify({
      type: 'FINALIZE_QUEUE',
      content: value
    }));

    setPending(true);
  }

  const pausingQueue = async (id: string) => {
    const value ={
      sub,
      queue: id
    }

    socket.send(JSON.stringify({
      type: 'PAUSE_QUEUE',
      content: value
    }));

    setPending(true);
  }

  const resumingQueue = async (id: string) => {
    const value ={
      sub,
      queue: id
    }

    socket.send(JSON.stringify({
      type: 'RESUME_QUEUE',
      content: value
    }));

    setPending(true);
  }

  const queueStringGenerated = () : string => {
    let arrayEle = new Array();

    arrayEle.push('queue');
    arrayEle.push(item.id);

    const result = arrayEle.toString();

    return result
  }


  const generateLinking = () : string => {
    const linking = Linking.createURL('processItem', {
      queryParams: { data: queueStringGenerated() },
    });

    const url = LINKING_URL + linking;

    return url
  } 

  if(pending){
    return <Loading />
  }

  const meanTime = ( createdAt: number, elapsedTime: number, completed: number) : string => {

    if(completed == 0)
      return I18n.t('general.calculating');

    const absDiffMilliSeconds = Math.abs(elapsedTime - createdAt) / 1000

    let meanMilliSeconds = absDiffMilliSeconds / completed;

    // calculate days
    const days = Math.floor(meanMilliSeconds / 86400);
    meanMilliSeconds -= days * 86400;

    // calculate hours
    const hours = Math.floor(meanMilliSeconds / 3600) % 24;
    meanMilliSeconds -= hours * 3600;

    // calculate minutes
    const minutes = Math.floor(meanMilliSeconds / 60) % 60;
    meanMilliSeconds -= minutes * 60;

    let difference = '';
    if (days > 0) {
      difference += (days === 1) ? `${days} ${I18n.t('general.day')}, ` : `${days} ${I18n.t('general.days')} `;
    }

    if(hours === 0 && minutes === 0){
      return I18n.t('general.leastOneMin')
    }

    difference += (hours === 0 || hours === 1) ? `${hours} ${I18n.t('general.hour')} ` : `${hours} ${I18n.t('general.hours')} `;

    difference += (minutes === 0 || hours === 1) ? `${minutes} ${I18n.t('general.minutes')}` : `${minutes} ${I18n.t('general.minutes')}`; 

    return difference;

  }

  const durationQueue = ( time: number, maxTime: number) : string => {

    const totalHours = (maxTime *60*60*1000);
    const finalTime = time + totalHours;

    const dateNow = Date.now();
    let diffInMilliSeconds = Math.abs(finalTime - dateNow) / 1000;

    // calculate days
    const days = Math.floor(diffInMilliSeconds / 86400);
    diffInMilliSeconds -= days * 86400;

    // calculate hours
    const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
    diffInMilliSeconds -= hours * 3600;

    // calculate minutes
    const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
    diffInMilliSeconds -= minutes * 60;

    let difference = '';
    if (days > 0) {
      difference += (days === 1) ? `${days} ${I18n.t('general.day')}, ` : `${days} ${I18n.t('general.days')} `;
    }

    difference += (hours === 0 || hours === 1) ? `${hours} ${I18n.t('general.hour')} ` : `${hours} ${I18n.t('general.hours')} `;

    difference += (minutes === 0 || hours === 1) ? `${minutes} ${I18n.t('general.minutes')}` : `${minutes} ${I18n.t('general.minutes')}`; 

    return difference;
  }

  return (
    <View style={{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: Colors[colorScheme].background,
      width: '100%'
    }}
    >
      <View style={{
          // flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          // backgroundColor: 'rgb(18, 18, 18)',

          width: '100%',
          height: '10%'
        }}
      >
        <Text numberOfLines={1} ellipsizeMode='tail' style={{color: Colors[colorScheme].text, fontSize: 24, padding: 8,}}>{item.name || item.id}</Text>
        
      </View>
      <View style={{
        width: '100%',
        height: '70%',
        alignItems: 'center',
        justifyContent: 'center',
      }} >
          <Text style={{color: Colors[colorScheme].text, fontSize: 16, padding: 24,}}>{item.description}</Text>
          
          <View style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: Colors[colorScheme].background,
                width: '100%'
              }}
            >
              <View style={{
                flex: 1,
                alignItems: 'flex-start',
                justifyContent: 'center',
                backgroundColor: Colors[colorScheme].background,
                width: '80%',
                marginLeft: 24
              }}>
                <Text  style={{color: Colors[colorScheme].text, margin: 4}}>{I18n.t('detailQueue.state')} </Text>
                <Text  style={{color: Colors[colorScheme].text, margin: 4}}>{I18n.t('detailQueue.progress')} </Text>
                <Text  style={{color: Colors[colorScheme].text, margin: 4}}>{I18n.t('detailQueue.timeService')} </Text>
                <Text  style={{color: Colors[colorScheme].text, margin: 4}}>{I18n.t('detailQueue.waitingTime')} </Text>

              </View>

              <View style={{
                flex: 1,
                alignItems: 'flex-end',
                justifyContent: 'center',
                backgroundColor: Colors[colorScheme].background,
                width: '80%',
                marginRight: 24
              }}>
                <Text  style={{color: Colors[colorScheme].text, margin: 4}}> {item.state} </Text>
                <Text style={{color: Colors[colorScheme].text, margin: 4}}>{item.completed} / {item.availability}</Text>
                <Text style={{color: Colors[colorScheme].text, margin: 4}}>{durationQueue(item.createdAt, item.maxTime)} </Text>
                <Text style={{color: Colors[colorScheme].text, margin: 4}}>{meanTime(item.createdAt, item.elapsedTime, item.completed)}</Text>
              </View>

          </View>
          {/* <View style={{width: '100%', alignItems: 'center', justifyContent: 'center',}}>
            <Pressable
              onPress={()=>navigation.navigate('ProcessScanScreen')}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
                margin: 8,
                width: '85%',
                borderRadius: 20,
                padding: 8,
                backgroundColor: Colors[colorScheme].buttonSuccess
              })}>
                <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}> {I18n.t('detailQueue.share')}</Text>
            </Pressable>
          </View> */}

          <ShareLinkButton 
            titleWindow="" 
            buttonText={I18n.t('detailQueue.share')} 
            buttonType="buttonSuccess"
            url={generateLinking()}
            message={generateLinking()}
          />
      </View>

      
      <View style={{
          // flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          // backgroundColor: 'rgb(18, 18, 18)',
          width: '100%',
          height: '20%',
          flexDirection: 'row',
        }}
      >
          {
            (item.state == 'resume' )?
              <Pressable
                onPress={()=> pausingQueue(item.id)}
                style={({ pressed }) => ({
                      opacity: pressed ? 0.5 : 1,
                      margin: 8,
                      width: '35%',
                      borderRadius: 30,
                      padding: 16,
                      backgroundColor: Colors[colorScheme].buttonSuccess
                })}>
                <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}> {I18n.t('detailQueue.pause')} </Text>
              </Pressable>
              : null 
          }

          {
            (item.state == 'paused' )?
              <Pressable
                onPress={()=> resumingQueue(item.id)}
                style={({ pressed }) => ({
                  opacity: pressed ? 0.5 : 1,
                  margin: 8,
                  width: '35%',
                  borderRadius: 30,
                  padding: 16,
                  backgroundColor: Colors[colorScheme].buttonSuccess
              })}>
                  <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}> {I18n.t('detailQueue.resume')} </Text>
              </Pressable>
              : null
          }  
      
          <Pressable
            onPress={()=> finalizeQueue(item.id)}
            style={({ pressed }) => ({
              opacity: pressed ? 0.5 : 1,
              margin: 8,
              width: '35%',
              borderRadius: 30,
              padding: 16,
              backgroundColor: Colors[colorScheme].buttonDanger
            })}>
              <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}> {I18n.t('detailQueue.finish')} </Text>
          </Pressable>
        </View>
    </View>
  );
}
