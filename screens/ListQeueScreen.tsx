import { StatusBar } from 'expo-status-bar';
import { Fragment, useState, useContext, useEffect} from 'react';
import { Platform, StyleSheet, ScrollView, View, Text, VirtualizedList, Pressable, Alert  } from 'react-native';
import * as Network from 'expo-network';
import { useDispatch, useSelector } from 'react-redux'
import Loading from '../components/Loading';
import Colors from '../constants/Colors';
import {toastSuccess, toastError} from '../utils/toast';
import {backendMessagesDecoded} from '../utils/messagesFormBackend';
import { useNavigation } from '@react-navigation/native';
import {Picker} from '@react-native-picker/picker';
import {SocketContext} from '../context/socket';
import {removeQueue, listQueue} from '../store/actions';
import I18n from '../i18n/locales';

export default function ListQeueScreen({ navigation, route}) {
  const socket = useContext(SocketContext);
  const [type, setType] = useState('resume');
  const [pending, setPending] = useState(false);

  const dispatch = useDispatch();
  const sub = useSelector(state => state.sub);
  const queues = useSelector(state => state.queues);
  const colorScheme = useSelector(state => state.theme);
  const lang = useSelector(state => state.lang);

  I18n.locale = lang; 

  useEffect(() => {
    const queueFinalize = (data: any) => {

      const res = JSON.parse(data);
      if(!res.ok){
        toastError(backendMessagesDecoded(res.msg));
      }
      else{
        dispatch(removeQueue(res.payload))
        setPending(false);
        toastSuccess(backendMessagesDecoded(res.action));
      }
    };

    const queueList = (data: any) => {

      const res = JSON.parse(data);
      if(!res.ok){
        toastError(backendMessagesDecoded(res.msg));
      }
      else {
        dispatch(listQueue(res.payload))
        setPending(false);
      }
    };

    const gotQueue = (data: any) => {

      const res = JSON.parse(data);
      if(!res.ok){
        toastError(backendMessagesDecoded(res.msg));
      }
      else{
        setPending(false);
        navigation.navigate('DetailQeueScreen', { res})
      }
    };

    listingQueue(type);
    
    socket.on('FINALIZED_QUEUE', queueFinalize);
    socket.on('LISTED_QUEUE', queueList);
    socket.on('GOT_QUEUE', gotQueue);

    return () => {
      socket.off('FINALIZED_QUEUE', queueFinalize),
      socket.on('LISTED_QUEUE', queueList);
      socket.off('GOT_QUEUE', gotQueue);
    };
  }, [socket]);

  const listingQueue = async (type: string) => {
    const value ={
      sub,
      type
    }

    socket.send(JSON.stringify({
      type: 'LIST_QUEUE',
      content: value
    }));

    setPending(true);
  }

  const finalizeQueue = async (id: string) => {
    const value ={
      sub,
      queue: id
    }

    socket.send(JSON.stringify({
      type: 'FINALIZE_QUEUE',
      content: value
    }));

    setPending(true);
  }

  const getQueue = async (id: string) => {
    const value ={
      sub,
      queue: id
    }

    socket.send(JSON.stringify({
      type: 'GET_QUEUE',
      content: value
    }));

    setPending(true);
  }

  if(pending)
    return <Loading />

const removeItemDialog = (id: string) => {
  if(Platform.OS == 'web'){
    finalizeQueue(id);
  }
  else{
   return Alert.alert(I18n.t('alert.title_queue'), I18n.t('alert.text_queue'), [
      {
        text: I18n.t('alert.button_cancel'),
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      { text: I18n.t('alert.button_accept_queue'), onPress: () => finalizeQueue(id), style:'destructive' },
    ]);
  }
}
    

const getItem = (data, index) => (
  {
    id: data[index].id,
    name: data[index].name,
    state: data[index].description,
    key: `${index + 1}`
  }
);

const getItemCount = (data) => data.length;

const Item = ({ id, name, state }) => (
  
      <View 
        key={id}
        style={{
          width: '100%',
          padding: 8,
          flex: 1,
          flexDirection: 'row'
        }}
      >
        
        <View style={{
          width: '80%',
          padding: 8
        }}>
          <Pressable
              onPress={() => getQueue(id)}
              //onPress={() => alert('sub ' + id)}
              style={
                ({ pressed }) => ({
                  opacity: pressed ? 0.5 : 1,
            })}>
              <Text numberOfLines={1} ellipsizeMode='tail' 
                style={{
                  color: Colors[colorScheme].text,
                  fontSize: 16
                }}
              >
                {name}
              </Text>
              <Text numberOfLines={1} ellipsizeMode='tail' 
                style={{
                  color: Colors[colorScheme].text,
                  fontSize: 12
                }}
              >
                {state}
              </Text>
          </Pressable>
        </View>
        <View style={{
          width: '20%',
          paddingTop: 8,
          paddingBottom: 8,
        }}>
          <Pressable
            onPress={() => removeItemDialog(id)}
            style={
              ({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
                backgroundColor: Colors[colorScheme].buttonDanger,
                padding: 4,
                width: '100%',
                // top: 10,
                // right: 8,
                height: 32,
                borderRadius: 20
          })}>
            <Text style={{
                color: '#fff',
                fontSize: 14,
                textAlign: 'center'
              }}>
                {I18n.t('listQueue.finish')}
            </Text>
        </Pressable>

        </View>
        
    </View>
);

  return (
    <View 
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors[colorScheme].background,
        width: '100%'
      }}
    >
        <View style={{
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'stretch',
            marginLeft: '3%',
            marginTop: 8,
            width: '94%',
            color: Colors[colorScheme].colorInput,
            backgroundColor: Colors[colorScheme].backgroundInput,
            borderColor: Colors[colorScheme].borderInput,
            borderWidth: 1,
            height: 58,
            borderRadius: 20
          }}
        >
            <Picker
              selectedValue={type}
              onValueChange={(itemValue, itemIndex) => {
                setType(itemValue);
                listingQueue(itemValue);
              }}
              style={{
                position:'relative',
                marginLeft: '2%',
                marginTop: 4,
                width: '90%',
                height: 40,
                color: Colors[colorScheme].colorInput,
                backgroundColor: Colors[colorScheme].backgroundInput,
                // borderColor: Colors[colorScheme].borderInput,
                borderWidth: 0,
                // borderRadius: 20
              }}
            >
            <Picker.Item  label={I18n.t('listQueue.active')} value="resume" />
            <Picker.Item label={I18n.t('listQueue.pause')} value="paused" />
          </Picker>

        </View>
        
        {
          queues.length > 0 ? (
            <ScrollView
              style={{
                width: '100%',
              }}
             >
              <VirtualizedList
                  data={queues}
                  initialNumToRender={4}
                  renderItem={({ item }) => <Item name={item.name} state={item.state} id={item.id}/>}
                  keyExtractor={item => item.id}
                  getItemCount={getItemCount}
                  getItem={getItem}

                  
              />
              
            </ScrollView>
            
          )
          :
          (
            <View style={{
               // flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                // backgroundColor: 'rgb(18, 18, 18)',
      
                width: '100%',
                height: '90%'
              }}
            >
              <Text style={{color: Colors[colorScheme].text, fontSize: 16, textAlign: 'center'}}> 
                {I18n.t('listQueue.empty')}
              </Text>
             
            </View>
          )
        }
      </View>
  );
}
