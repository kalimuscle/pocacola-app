import { StatusBar } from 'expo-status-bar';
import { Fragment, useState, useContext, useEffect} from 'react';
import { Platform, StyleSheet, ScrollView, View, Text, VirtualizedList, Pressable, Alert  } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'
import Loading from '../components/Loading';
import Colors from '../constants/Colors';
import {toastSuccess, toastError} from '../utils/toast';
import {backendMessagesDecoded} from '../utils/messagesFormBackend';
import {Picker} from '@react-native-picker/picker';
import {SocketContext} from '../context/socket';
import {removeTicket, listTicket} from '../store/actions';
import I18n from '../i18n/locales';

export default function ListTicketScreen({ navigation, route}) {
  const socket = useContext(SocketContext);
  const [type, setType] = useState('active');
  const [pending, setPending] = useState(false);

  const dispatch = useDispatch();
  const sub = useSelector(state => state.sub);
  const tickets = useSelector(state => state.tickets);

  const colorScheme = useSelector(state => state.theme);
  const lang = useSelector(state => state.lang);

  I18n.locale = lang; 

  useEffect(() => {
    const ticketGiveUp = (data: any) => {

      const res = JSON.parse(data);
      if(!res.ok){
        toastError(backendMessagesDecoded(res.msg));
      }
      else{
        dispatch(removeTicket(res.payload))
        setPending(false);
        toastSuccess(backendMessagesDecoded(res.action));
      }
    };

    const ticketList = (data: any) => {

      const res = JSON.parse(data);
      if(!res.ok){
        toastError(backendMessagesDecoded(res.msg));
      }
      else {
        dispatch(listTicket(res.payload))
        setPending(false);
      }
    };

    const gotTicket = (data: any) => {

      const res = JSON.parse(data);
      if(!res.ok){
        toastError(backendMessagesDecoded(res.msg));
      }
      else{
        setPending(false); console.log(res);
        navigation.navigate('DetailTicketScreen', { res})
      }
    };

    listingTicket(type);

    socket.on('GAVE_UP_TICKET', ticketGiveUp);
    socket.on('LISTED_TICKET', ticketList);
    socket.on('GOT_TICKET', gotTicket);

    return () => {
      socket.off('GAVE_UP_TICKET', ticketGiveUp),
      socket.on('LISTED_TICKET', ticketList);
      socket.off('GOT_TICKET', gotTicket);
    };
  }, [socket]);

  const listingTicket = async (type: string) => {
    const value ={
      sub,
      type
    }

    socket.send(JSON.stringify({
      type: 'LIST_TICKET',
      content: value
    }));

    setPending(true);
  }

  const giveUpTicket = async (id: string) => {
    const value ={
      sub,
      ticket: id
    }

    socket.send(JSON.stringify({
      type: 'GIVE_UP_TICKET',
      content: value
    }));

    setPending(true);
  }

  const getTicket = async (id: string) => {
    const value ={
      sub,
      ticket: id
    }

    socket.send(JSON.stringify({
      type: 'GET_TICKET',
      content: value
    }));

    setPending(true);
  }

  if(pending)
    return <Loading />

const removeItemDialog = (id: string) => {
  if(Platform.OS == 'web'){
    giveUpTicket(id);
  }
  else{
   return Alert.alert(I18n.t('alert.title_ticket'), I18n.t('alert.text_ticket'), [
      {
        text: I18n.t('alert.button_cancel'),
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      { text: I18n.t('alert.button_accept_ticket'), onPress: () => giveUpTicket(id), style:'destructive' },
    ]);
  }
}
    
const getItem = (data, index) => (
  {
    id: data[index].id,
    queuePosition: data[index].queuePosition,
    name: data[index].name,
    state: '25/100',
    key: `${index + 1}`
  }
);

const getItemCount = (data) => data.length;

const Item = ({ id, name, state, position }) => (
  
      <View 
        key={id}
        style={{
          width: '100%',
          padding: 8,
          flex: 1,
          flexDirection: 'row'
        }}
      >
        <View style={{
          flex: 1,
          width: '15%',
          padding: 8
        }}>
            <Text style={{
                  color: Colors[colorScheme].text,
                  fontSize: 12,
                  paddingTop: 8,
                  paddingBottom: 8,
              }}>
                 #{position} 
            </Text>

        </View>
        
        <View style={{
          
          width: '60%',
          padding: 8
        }}>
          <Pressable
              onPress={() => getTicket(id)}
              //onPress={() => alert('sub ' + id)}
              style={
                ({ pressed }) => ({
                  opacity: pressed ? 0.5 : 1,
            })}>
              <Text numberOfLines={1} ellipsizeMode='tail' 
                style={{
                  color: Colors[colorScheme].text,
                  fontSize: 16
                }}>
                  {name}
              </Text>
              <Text style={{
                  color: Colors[colorScheme].text,
                  fontSize: 12
                }}>
                 {I18n.t('listTicket.state')} {state}
              </Text>
          </Pressable>
        </View>
        <View style={{
          width: '25%',
          paddingTop: 8,
          paddingBottom: 8,
        }}>
          <Pressable
            onPress={() => removeItemDialog(id)}
            style={
              ({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
                backgroundColor: Colors[colorScheme].buttonDanger,
                padding: 4,
                width: '100%',
                // top: 10,
                // right: 8,
                height: 32,
                borderRadius: 20
          })}>
            <Text style={{
                color: '#fff',
                fontSize: 14,
                textAlign: 'center'
              }}>
                {I18n.t('listTicket.abandom')}
            </Text>
        </Pressable>

        </View>
        
    </View>
);

  return (
    <View 
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors[colorScheme].background,
        width: '100%'
      }}
    >
        <View style={{
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'stretch',
            marginLeft: '3%',
            marginTop: 8,
            width: '94%',
            color: Colors[colorScheme].colorInput,
            backgroundColor: Colors[colorScheme].backgroundInput,
            borderColor: Colors[colorScheme].borderInput,
            borderWidth: 1,
            height: 58,
            borderRadius: 20
          }}
        >
            <Picker
              selectedValue={type}
              onValueChange={(itemValue, itemIndex) => {
                setType(itemValue);
                listingTicket(itemValue);
              }}
              style={{
                position:'relative',
                marginLeft: '2%',
                marginTop: 4,
                //paddingLeft: 16,
                width: '90%',
                height: 40,
                color: Colors[colorScheme].colorInput,
                backgroundColor: Colors[colorScheme].backgroundInput,
                // borderColor: Colors[colorScheme].borderInput,
                borderWidth: 0,
                // borderRadius: 20
              }}
            >
              <Picker.Item label={I18n.t('listTicket.active')} value="active" />
              <Picker.Item label={I18n.t('listTicket.completed')} value="completed" />
              <Picker.Item label={I18n.t('listTicket.abandoned')} value="abandoned" />
            </Picker>

        </View>
        
        {
          tickets.length > 0 ? (
            <ScrollView
              style={{
                width: '100%',
              }}
             >
              <VirtualizedList
                data={tickets}
                initialNumToRender={4}
                renderItem={({ item }) => <Item name={item.name} state={item.state} id={item.id} position={item.queuePosition}/>}
                keyExtractor={item => item.id}
                getItemCount={getItemCount}
                getItem={getItem}
              />
              
            </ScrollView>
            
          )
          :
          (
            <View style={{
               // flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                // backgroundColor: 'rgb(18, 18, 18)',
      
                width: '100%',
                height: '90%'
              }}
            >
              <Text style={{color: Colors[colorScheme].text, fontSize: 16, textAlign: 'center'}}>{I18n.t('listTicket.empty')}</Text>
            </View>
          )
        }
      </View>
  );
}
