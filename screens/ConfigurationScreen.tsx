import { StatusBar } from 'expo-status-bar';
import {useState, useEffect } from 'react';
import { Platform, StyleSheet, View, Text, Pressable, ScrollView  } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'
// import { BarCodeScanner } from 'expo-barcode-scanner';

import Colors from '../constants/Colors';
import I18n from '../i18n/locales';

import { useNavigation } from '@react-navigation/native';
import {Picker} from '@react-native-picker/picker';
import {themeUpdate, languageChange} from '../store/actions';

export default function ConfigurationScreen() {
  const lang = useSelector(state => state.lang);
  const colorScheme = useSelector(state => state.theme);
  //const colorScheme = useColorScheme();
  const navigation = useNavigation();

  const dispatch = useDispatch();
  I18n.locale = lang; 

  return (
    <View 
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors[colorScheme].background,
        width: '100%'
      }}
    >
      <View style={{
            // flex: 1,
            alignItems: 'flex-start',
            justifyContent: 'center',
            backgroundColor: Colors[colorScheme].background,
      
             width: '100%',
            height: '10%'
          }}
        ></View>
      <View style={{
            // flex: 1,
            alignItems: 'flex-start',
            justifyContent: 'center',
            backgroundColor: Colors[colorScheme].background,
      
             width: '100%',
            height: '60%'
          }}
        >
          <Text style={{padding: 16, fontSize:16, textAlign: 'left', marginTop: 4, marginBottom: 4, color: Colors[colorScheme].text}}>
            {I18n.t('configuration.language')}
          </Text>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'stretch',
              marginLeft: '3%',
              width: '94%',
              color: Colors[colorScheme].colorInput,
              backgroundColor: Colors[colorScheme].backgroundInput,
              borderColor: Colors[colorScheme].borderInput,
              borderWidth: 1,
              height: 58,
              borderRadius: 20
          }}>
            <Picker
              selectedValue={lang}
              onValueChange={(itemValue, itemIndex) => {
                dispatch(languageChange(itemValue));
              }}
              style={{
                position:'relative',
                marginLeft: '2%',
                //paddingLeft: 16,
                width: '90%',
                height: 42,
                color: Colors[colorScheme].colorInput,
                backgroundColor: Colors[colorScheme].backgroundInput,
                // borderColor: Colors[colorScheme].borderInput,
                borderWidth: 0,
                // borderRadius: 20
              }}
            >
              <Picker.Item label={I18n.t('configuration.es')} value="es" />
              <Picker.Item label={I18n.t('configuration.en')} value="en" />
              <Picker.Item label={I18n.t('configuration.de')} value="de" />
              {/* <Picker.Item label={I18n.t('configuration.du')} value="du" /> */}
            </Picker>
            </View>
            <Text style={{padding: 16, fontSize:16, textAlign: 'left', marginTop: 4, marginBottom: 4, color: Colors[colorScheme].text}}>
            {I18n.t('configuration.theme')}
          </Text>
          <View
            style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'stretch',
                marginLeft: '3%',
                width: '94%',
                color: Colors[colorScheme].colorInput,
                backgroundColor: Colors[colorScheme].backgroundInput,
                borderColor: Colors[colorScheme].borderInput,
                borderWidth: 1,
                height: 58,
                borderRadius: 20
          }}>
            <Picker
              selectedValue={colorScheme}
              onValueChange={(itemValue, itemIndex) => dispatch(themeUpdate(itemValue))}
              style={{
                position:'relative',
                marginLeft: '2%',
                //paddingLeft: 16,
                width: '90%',
                height: 42,
                color: Colors[colorScheme].colorInput,
                backgroundColor: Colors[colorScheme].backgroundInput,
                // borderColor: Colors[colorScheme].borderInput,
                borderWidth: 0,
                // borderRadius: 20
              }} 
            >
              <Picker.Item label={I18n.t('configuration.dark')} value="dark" />
              <Picker.Item label={I18n.t('configuration.light')} value="light" />
            </Picker>
          </View>
      </View>
      <View style={{
            // flex: 1,
            alignItems: 'flex-start',
            justifyContent: 'center',
            backgroundColor: Colors[colorScheme].background,
      
             width: '100%',
            height: '30%'
          }}
        ></View>
       
    </View> 
  
  );
}
