import { StatusBar } from 'expo-status-bar';
import {useState, useLayoutEffect, useEffect} from 'react';
import { 
  Platform, 
  StyleSheet, 
  View, 
  Image, 
  SafeAreaView, 
  Text,
  Pressable, 
  ActivityIndicator,
  I18nManager
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux'
//import { Button, Icon, Layout, Spinner, Text } from '@ui-kitten/components';
import { Entypo } from '@expo/vector-icons';
import { hideSplash } from '../store/actions';
import ShareLinkButton from '../components/ShareLinkButton';
import logo from '../assets/images/splash.png'; 
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import {generateGuestUser} from '../store/actions';
import { save, getValueFor} from '../utils/secure-store';
import I18n from '../i18n/locales';
import * as Linking from 'expo-linking';

export default function QeueScreen({ navigation, route}) {
  const showSplash = useSelector(state => state.showSplash);

  const colorScheme = useSelector(state => state.theme);
  const dispatch = useDispatch();
  const sub = useSelector(state => state.sub);
  const lang = useSelector(state => state.lang);

  I18n.locale = lang; 

  useEffect(() => {
  
    if(sub === undefined || sub === ''){
      dispatch(generateGuestUser());
    }

    navigation.setOptions({
      headerShown: showSplash  ? false : true,
    });
  }, []);
    return (
      <SafeAreaView style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors[colorScheme].background
      }}>
          <View 
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: Colors[colorScheme].background
            }}
          >
          </View>
          <View
             style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'transparent'
            }}
          >
            <Text style={{color: Colors[colorScheme].text, fontSize: 24}}>{I18n.t('homepage.welcome')}</Text>
            <View style={styles.buttonContainer}>

              <Pressable
                onPress={() => navigation.navigate('CreateQeueScreen')}
                style={({ pressed }) => ({
                  opacity: pressed ? 0.5 : 1,
                  margin: 8,
                  width: 135,
                  borderRadius: 30,
                  padding: 16,
                  backgroundColor: Colors[colorScheme].buttonSuccess
                })}>
                  <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}> {I18n.t('homepage.button1')}</Text>
              </Pressable>

              <Pressable
                onPress={() => navigation.navigate('QRScanQeueScreen')}
                style={({ pressed }) => ({
                  opacity: pressed ? 0.5 : 1,
                  margin: 8,
                  width: 135,
                  borderRadius: 30,
                  padding: 16,
                  backgroundColor: Colors[colorScheme].buttonInfo
                })}>
                  <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}> {I18n.t('homepage.button2')} </Text>
              </Pressable>
            </View>
          </View>
          <View
             style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: Colors[colorScheme].background
            }}
          >
          </View>
          <ShareLinkButton 
            buttonType="buttonDanger" 
            titleWindow={I18n.t('homepage.invitationText')} 
            buttonText={I18n.t('homepage.inviteButton')}
            url={`${Linking.createURL('/')}createQeue`}
            message={`${Linking.createURL('/')}createQeue`}
          />
      </SafeAreaView>
    );
  
}

const styles = StyleSheet.create({
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'none'
  },
  splashText: {
    color: '#fff',
    fontSize: 24,
  },
  mainText: {
    color: '#fff',
    fontSize: 24,
  },
  shareText: {
    marginBottom: 4,
    color: '#fff'
  },
  button: {
    margin: 8,
    width: 135
  },
  buttonInvitation: {
    margin: 4,
    height: 45
    //width: '100%',
    // backgroundColor: 'rgb(51, 102, 255)',
    // borderColor: 'rgb(51, 102, 255)'
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
