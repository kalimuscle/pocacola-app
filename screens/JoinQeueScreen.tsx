import { StatusBar } from 'expo-status-bar';
import { useDispatch, useSelector } from 'react-redux';
import {useState, useEffect, useContext} from 'react';
import { Platform, StyleSheet, View, Text, Pressable, TextInput  } from 'react-native';
import {SocketContext} from '../context/socket';
import {addTicket} from '../store/actions';
import {toastSuccess, toastError} from '../utils/toast';
import {backendMessagesDecoded} from '../utils/messagesFormBackend';
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import {FontAwesome } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import I18n from '../i18n/locales';

export default function JoinQeueScreen() {
  const socket = useContext(SocketContext);
  const [value, setValue] = useState('');
  const colorScheme = useSelector(state => state.theme);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const sub = useSelector(state => state.sub);
  const [pending, setPending] = useState(false);

  const lang = useSelector(state => state.lang);

  I18n.locale = lang; 

  useEffect(() => {
    const queueJoined = (data: any) => {
      const res = JSON.parse(data);
      if(!res.ok){
        toastError(backendMessagesDecoded(res.msg));
      }
      else{
        setPending(false);
        dispatch(addTicket(res.payload))
        toastSuccess(backendMessagesDecoded(res.action));
      }
    };
  
    socket.on('JOINED_QUEUE', queueJoined);

    return () => {
      socket.off('JOINED_QUEUE', queueJoined);
    };
  }, [socket]);

  const joinQueue = async () => {

    socket.send(JSON.stringify({
      type: 'JOIN_QUEUE',
      content: {
        sub,
        queue: value
      }
    }));

    setPending(true);
  }

  return (
    <View style={{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      //height: 100,
      backgroundColor: Colors[colorScheme].background
    }}>
     
      <View style={{
          // flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          // backgroundColor: 'rgb(18, 18, 18)',

          width: '100%',
          height: '80%'
        }}
      >
        <View style={{
          width: '100%', 
          alignItems: 'flex-start',
          justifyContent: 'center',
          flexDirection: 'column',
          padding: 8
        }}>
          <Text style={{color: Colors[colorScheme].text, margin: 16, textAlign: 'left'}}> {I18n.t('joinQueue.code')} </Text>
          <TextInput
              style={{
                padding: 8,
                width: '90%',
                marginLeft: '5%',
                marginRight: '5%',
                color: Colors[colorScheme].colorInput,
                backgroundColor: Colors[colorScheme].backgroundInput,
                borderColor: Colors[colorScheme].borderInput,
                borderWidth: 1,
                borderRadius: 20
              }}
              value={value}
              keyboardType="default"
              onChangeText={nextValue => setValue(nextValue)}
          />
        </View>

        </View>
        <View style={{
          // flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          // backgroundColor: 'rgb(18, 18, 18)',
          width: '100%',
          height: '20%'
        }}>
          <Pressable
            onPress={()=> joinQueue()}
            disabled={ value.length == 0}
            style={({ pressed }) => ({
              opacity: pressed ? 0.5 : 1,
              margin: 8,
              width: '85%',
              borderRadius: 30,
              padding: 16,
              backgroundColor: Colors[colorScheme].buttonSuccess
            })}>
              <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}> {I18n.t('joinQueue.button')} </Text>
          </Pressable>
        </View>
    </View>
  );
}