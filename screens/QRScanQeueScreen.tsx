import { StatusBar } from 'expo-status-bar';
import {useState, useEffect } from 'react';
import { Platform, StyleSheet, View, Text, Pressable, Alert  } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'
import { BarCodeScanner } from 'expo-barcode-scanner';

import Loading from '../components/Loading';
import { requestCameraPermission } from '../store/actions';
import {FontAwesome } from '@expo/vector-icons';
import Colors from '../constants/Colors';
import { useNavigation } from '@react-navigation/native';
import Toast from 'react-native-root-toast';
import I18n from '../i18n/locales';

export default function QRCodeQeueScreen({ route}) {

  const [scanned, setScanned] = useState(false);

  const hasPermission = useSelector(state => state.cameraPermission)

  const dispatch = useDispatch();
  const colorScheme = useSelector(state => state.theme);
  const navigation = useNavigation();

  const lang = useSelector(state => state.lang);

  I18n.locale = lang; 

  if(Platform.OS == 'web'){
    navigation.navigate('JoinQeueScreen');
  }

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      dispatch(requestCameraPermission(status === 'granted'));
    })();
  }, []);

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
    navigation.navigate('ProcessScanScreen', { type, data})
  };

  if (hasPermission === null) {
    return <Loading/>;
  }
  if (hasPermission === false) {
    return (
      <View style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors[colorScheme].background,
        width: '100%'
      }}
      >
            <Pressable
              onPress={()=> navigation.navigate('JoinQeueScreen')}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
                margin: 8,
                width: '85%',
                borderRadius: 30,
                padding: 16,
                backgroundColor: Colors[colorScheme].buttonSuccess
              })}>
                <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}> {I18n.t('qrScan.codeQueue')} </Text>
            </Pressable>
        
      </View>
    );
  }

  return (
    <View style={{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: Colors[colorScheme].background,
      width: '100%'
    }}
    >
  
      <View style={{
        width: '100%',
        height: '80%'
      }} >
          
          <View style={{width: '100%',height: '15%',}} ></View>
          <View style={{width: '100%',height: '70%',}} >
            <BarCodeScanner
              onBarCodeScanned={scanned ? undefined : handleBarCodeScanned} 
              barCodeTypes={[BarCodeScanner.Constants.BarCodeType.qr]}
              style={StyleSheet.absoluteFillObject}
            />
          </View>

          <View style={{ width: '100%', height: '15%',}}></View>
      </View>
      <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%',
          height: '20%'
        }}
      >
        {
         scanned &&
          <Pressable
                onPress={()=> setScanned(false)}
                style={({ pressed }) => ({
                  opacity: pressed ? 0.5 : 1,
                  margin: 8,
                  width: '85%',
                  borderRadius: 30,
                  padding: 16,
                  backgroundColor: Colors[colorScheme].buttonSuccess
                })}
          >
              <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}> {I18n.t('qrScan.scanAgain')}</Text>
          </Pressable>
        }
          
      </View>
    </View>
  );
}
