import { Platform, StyleSheet, View, Text, Pressable  } from 'react-native';
import { Fragment, useState, useContext, useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Loading from '../components/Loading';
import StateOperation from '../components/StateOperation';
import Colors from '../constants/Colors';
import {toastSuccess, toastError} from '../utils/toast';
import {backendMessagesDecoded} from '../utils/messagesFormBackend';
import {SocketContext} from '../context/socket';
import I18n from '../i18n/locales';

export default function ProcessScanScreen({ route}) {
  const {type, data} = route.params;
  const socket = useContext(SocketContext);
  const [pending, setPending] = useState(true);
  const [state, setState] = useState(false);

  const dispatch = useDispatch();
  const sub = useSelector(state => state.sub);

  const lang = useSelector(state => state.lang);
  I18n.locale = lang; 

  const colorScheme = useSelector(state => state.theme);

  useEffect(() => {
    const processedScan = (data: any) => {

      const res = JSON.parse(data);
      setState(res.ok);
      if(!res.ok){
        toastError(backendMessagesDecoded(res.msg));
      }
      else{
        setPending(false);
        toastSuccess(backendMessagesDecoded(res.action));
      }
    };

    socket.on('JOINED_QUEUE', processedScan);
    socket.on('VERIFIED_TICKET', processedScan);
    socket.on('DONATED_TICKET', processedScan);

    return () => {
      socket.off('JOINED_QUEUE', processedScan),
      socket.off('VERIFIED_TICKET', processedScan);
      socket.off('DONATED_TICKET', processedScan);
    };
  }, [socket]);

  const processing = async (type: string, data: string) => {

    const items = data.split(',');

    switch(items[0]){
      case 'queue':
        socket.send(JSON.stringify({
          type: 'JOIN_QUEUE',
          content: {
            sub,
            queue: items[1]
          }
        }));
        break;

      case 'ticket':
        socket.send(JSON.stringify({
          type: 'VERIFY_TICKET',
          content: {
            sub,
            ticket: items[1],
            owner: items[3]
          }
        }));
        break;

      case 'donate':
        socket.send(JSON.stringify({
          type: 'DONATE_TICKET',
          content: {
            sub,
            ticket: items[1],
            owner: items[3]
          }
        }));
        break;
    }
  }

  processing(type,data);

  if(pending)
    return <Loading />

  return (
    <View style={{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: Colors[colorScheme].background,
      width: '100%'
    }}
    >
      {
        type == 'queue' ? (<StateOperation state messageFail={I18n.t('processScan.errorQueue')} messageSuccesful={I18n.t('processScan.successQueue')}/>) : null
      }

      {
        type == 'ticket' ? (<StateOperation state messageFail={I18n.t('processScan.errorTicket')} messageSuccesful={I18n.t('processScan.successTicket')}/>) : null
      }
      
      <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}> {type} {data} </Text>
    </View>
  );
}