import { StatusBar } from 'expo-status-bar';
import {useState, useEffect } from 'react';
import { Platform, StyleSheet, View, Text, Pressable, ScrollView, SafeAreaView, Image } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import {FontAwesome } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import logo from '../assets/images/splash.png'; 
//import logo from '../assets/images/queue.jpg'; 
import I18n from '../i18n/locales';

export default function SplashScreen() {
  const colorScheme = useSelector(state => state.theme);
  const lang = useSelector(state => state.lang);

  I18n.locale = lang; 
  const navigation = useNavigation();

  useEffect(async() => {
    await new Promise(resolve => setTimeout(resolve, 5000));
    navigation.navigate('QeueScreen');
  }, []);


  return (
    <SafeAreaView style={{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: Colors[colorScheme].background
    }}>
      <Image source={logo} 
        style={{ 
          width: '90%', 
          height: 159 
        }} 
      /> 
      <Text style={{
        color: Colors[colorScheme].text,
        fontSize: 24,
      }}>
        PocaCola
      </Text>
      <Text style={{
        color: Colors[colorScheme].text,
        fontSize: 24,
      }}>
        0.0.1
        </Text>
    </SafeAreaView>
  
  );
}
