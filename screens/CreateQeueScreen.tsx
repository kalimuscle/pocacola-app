import { StatusBar } from 'expo-status-bar';
import {useState, useContext, useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { Platform, StyleSheet, View, Alert, Pressable, Text, TextInput, ScrollView  } from 'react-native';
//import { Button, Icon, Layout, Spinner, Text, Input} from '@ui-kitten/components';
import ShareLinkButton from '../components/ShareLinkButton';
import Loading from '../components/Loading';
import {FontAwesome } from '@expo/vector-icons';
import Colors from '../constants/Colors';
import { useNavigation } from '@react-navigation/native';
import {toastSuccess, toastError} from '../utils/toast';

import {backendMessagesDecoded} from '../utils/messagesFormBackend';
import {SocketContext} from '../context/socket';
import {useNetInfo} from "@react-native-community/netinfo";
import I18n from '../i18n/locales';
import NotConnection from '../components/NotConnection';

let qeue = {};

export default function CreateQeueScreen() {
  const socket = useContext(SocketContext);
  const sub = useSelector(state => state.sub);
  // const [retrying, setRetrying] = useState(false);
  // const [conn, setConnection] = useState(true);

  const [name, setName] = useState('');
  const [detail, setDetail] = useState('');
  const [availability, setAvailability] = useState(100);
  const [duration, setDuration] = useState(10);
  const [pending, setPending] = useState(false);

  const dispatch = useDispatch();
  const colorScheme = useSelector(state => state.theme);
  const navigation = useNavigation();
  const lang = useSelector(state => state.lang);

  const netInfo = useNetInfo();


  I18n.locale = lang; 

  useEffect(() => {
    const queueCreated = (data: any) => {
      
      const res = JSON.parse(data);
      if(!res.ok){
        toastError(backendMessagesDecoded(res.msg));
      }
      else{
        setPending(false); 
        toastSuccess(backendMessagesDecoded(res.action));
        navigation.navigate('QRCodeQeueScreen', {
          data: res.payload,
          qrType: 'queue'
        });
      }
    };
  
    socket.on('CREATED_QUEUE', queueCreated);

    return () => {
      socket.off('CREATED_QUEUE', queueCreated);
    };
  }, [socket]);

  const createQueue = async () => {
    // const status = netInfo.isConnected !== null  && netInfo.isConnected && netInfo.isInternetReachable ? 
    // netInfo.isConnected && netInfo.isInternetReachable : false

    // setConnection(status);
    // setRetrying(false)

    // if(!status)
    //   return;
    
    qeue = {
      sub,
      name,
      detail,
      duration,
      availability
    };
    
    socket.send(JSON.stringify({
      type: 'CREATE_QUEUE',
      content: qeue
    }));

    setPending(true);
  }

  if(pending)
    return <Loading />

  return (
    <View style={{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      height: 100,
      backgroundColor: Colors[colorScheme].background
    }}>
      <ScrollView
              style={{
                width: '100%',
              }}
      >
      
        <View style={{
          width: '100%',
          height: '80%'
          }}
        >
            <View style={{
              width: '100%', 
              alignItems: 'flex-start',
              justifyContent: 'center',
              flexDirection: 'column',
              marginLeft: '7.5%',
              padding: 8
            }}>
                <Text style={{color: Colors[colorScheme].text, margin: 8, textAlign: 'left'}}> {I18n.t('createQueue.availability')}</Text>
                <TextInput
                    style={{
                      padding: 8,
                      width: '85%',
                      color: Colors[colorScheme].colorInput,
                      backgroundColor: Colors[colorScheme].backgroundInput,
                      borderColor: Colors[colorScheme].borderInput,
                      borderWidth: 1,
                      borderRadius: 20
                    }}
                    value={availability < 1 || availability > 10000 ? "1" : availability.toString() }
                   
                    maxLength={10000}
                    keyboardType="numeric"
                    onChangeText={nextValue => setAvailability(parseInt(nextValue))}
                />
            </View>
            <View style={{
              width: '100%', 
              alignItems: 'flex-start',
              justifyContent: 'center',
              flexDirection: 'column',
              marginLeft: '7.5%',
              padding: 8
            }}>
                <Text style={{color: Colors[colorScheme].text, margin: 8, textAlign: 'left'}}> {I18n.t('createQueue.duration')}</Text>
                <TextInput
                    style={{
                      padding: 8,
                      width: '85%',
                      color: Colors[colorScheme].colorInput,
                      backgroundColor: Colors[colorScheme].backgroundInput,
                      borderColor: Colors[colorScheme].borderInput,
                      borderWidth: 1,
                      borderRadius: 20
                    }}
                    value={duration < 1 || duration > 10000 ? "1" : duration.toString()}
                    
                    maxLength={10000}
                    keyboardType="numeric"
                    onChangeText={nextValue => setDuration(parseInt(nextValue))}
                />
            </View>
            <View style={{
              width: '100%', 
              alignItems: 'flex-start',
              justifyContent: 'center',
              flexDirection: 'column',
              marginLeft: '7.5%',
              padding: 8
            }}>
                <Text style={{color: Colors[colorScheme].text, margin: 8, textAlign: 'left'}}> {I18n.t('createQueue.name')}</Text>
                <TextInput
                    style={{
                      padding: 8,
                      width: '85%',
                      color: Colors[colorScheme].colorInput,
                      backgroundColor: Colors[colorScheme].backgroundInput,
                      borderColor: Colors[colorScheme].borderInput,
                      borderWidth: 1,
                      borderRadius: 20
                    }}
                    value={name}
                    maxLength={30}
                    keyboardType="default"
                    onChangeText={nextValue => setName(nextValue)}
                />
            </View>

            <View style={{
              width: '100%', 
              alignItems: 'flex-start',
              justifyContent: 'center',
              flexDirection: 'column',
              marginLeft: '7.5%',
              padding: 8
            }}>
                <Text style={{color: Colors[colorScheme].text, margin: 8, textAlign: 'left'}}> {I18n.t('createQueue.description')}</Text>
                <TextInput
                    style={{
                      padding: 8,
                      width: '85%',
                      //height: '200',
                      minHeight: 100,
                      color: Colors[colorScheme].colorInput,
                      backgroundColor: Colors[colorScheme].backgroundInput,
                      borderColor: Colors[colorScheme].borderInput,
                      borderWidth: 1,
                      borderRadius: 20
                    }}
                    value={detail}
                    multiline={true}

                    maxLength={140}
                    keyboardType="default"
                    onChangeText={nextValue => setDetail(nextValue)}
                />
            </View>
          </View>
        </ScrollView>
        <View style={{
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            height: '20%'
          }}
          >
          <Pressable
            onPress={()=> createQueue()}
            // onPress={()=> navigation.navigate('QRCodeQeueScreen')}
            disabled={ duration == 0 || availability == 0 || name.length == 0}
            style={({ pressed }) => ({
              opacity: pressed ? 0.5 : 1,
              margin: 8,
              width: '85%',
              borderRadius: 30,
              padding: 16,
              backgroundColor: Colors[colorScheme].buttonSuccess,
              
            })}>
              <Text style={{color: '#fff', fontSize: 16, textAlign: 'center'}}>  {I18n.t('createQueue.button')} </Text>
          </Pressable>
        </View>
        {/* <NotConnection show={!conn} onRetry={() =>{
          createQueue();
          setRetrying(true)
          }} isRetrying={retrying}/> */}
    </View>
    
  );
}