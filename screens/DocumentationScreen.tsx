import { StatusBar } from 'expo-status-bar';
import {useState, useEffect } from 'react';
import { Platform, StyleSheet, View, Text, Pressable, ScrollView  } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'
// import { BarCodeScanner } from 'expo-barcode-scanner';
import QRCode from 'react-native-qrcode-svg';
import Loading from '../components/Loading';
import ShareLinkButton from '../components/ShareLinkButton';
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import {FontAwesome } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import I18n from '../i18n/locales';

export default function DocumentationScreen() {
  const colorScheme = useSelector(state => state.theme);
  const navigation = useNavigation();
  const lang = useSelector(state => state.lang);

  I18n.locale = lang; 


  return (
    <ScrollView 
      style={{
        backgroundColor: Colors[colorScheme].background,
      }}
    >
      <View>
        <Text style={{padding: 16, fontSize:20, color: Colors[colorScheme].text}}>
          {I18n.t('documentation.header1')}
        </Text>  
      </View>

      <View>
        <Text style={{padding: 16, fontSize:16, textAlign: 'left', marginTop: 4, marginBottom: 4, color: Colors[colorScheme].text}}>
        
          {I18n.t('documentation.paragraph1')}
        </Text>
      </View>
      
      <View >
        <Text style={{padding: 16, fontSize:20, color: Colors[colorScheme].text}}>
          {I18n.t('documentation.header2')}
        </Text>  
      </View>

      <View>
        <Text style={{padding: 16, fontSize:16, textAlign: 'left', marginTop: 4, marginBottom: 4, color: Colors[colorScheme].text}}>
        
          {I18n.t('documentation.paragraph2')}
        </Text>

        <Text style={{padding: 16, fontSize:16, textAlign: 'left', marginTop: 4, marginBottom: 4, color: Colors[colorScheme].text}}>
          {I18n.t('documentation.paragraph3')}
        </Text>
        <Text style={{padding: 16, fontSize:16, textAlign: 'left', marginTop: 4, marginBottom: 4, color: Colors[colorScheme].text}}>
          {I18n.t('documentation.paragraph4')}
        </Text>
      </View>   
    </ScrollView> 
  
  );
}
