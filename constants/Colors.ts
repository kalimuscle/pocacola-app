const tintColorLight = '#000';
const tintColorDark = '#fff';

export default {
  light: {
    text: '#000',
    background: '#fbf4f4',
    tint: tintColorLight,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
    buttonSuccess: '#3366ff',
    buttonInfo: '#3bb547',
    buttonDanger: '#d03b63',
    colorInput: '#000',
    backgroundInput: '#eae3e3',
    borderInput: 'rgb(73, 73, 73)'
  },
  dark: {
    text: '#fff',
    background: '#000',
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
    buttonSuccess: '#3366ff',
    buttonInfo: '#3bb547',
    buttonDanger: '#d03b63',
    colorInput: '#000',
    backgroundInput: '#eae3e3',
    borderInput: '#fff'
  },
};
