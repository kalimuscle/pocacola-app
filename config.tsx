const SOCKET_ENDPOINT = 'https://f5a3-152-207-231-79.ngrok.io';
const LINKING_URL = 'https://f5a3-152-207-231-79.ngrok.io/linking?url=';

export  {
    SOCKET_ENDPOINT,
    LINKING_URL
}