export default {
    homepage: {
        title: 'Startseite',
        welcome: 'Willkommen auf PocaCola',
        button1: 'Schlange erstellen',
        button2: 'Schlange beitreten',
        invitationText: 'Freunde einladen um PocaCola beizutreten',
        inviteButton: 'Freunde einladen',
    },
    configuration: {
        title: 'Einstellungen',
        language: 'Sprache wählen',
        es: 'Spanisch',
        en: 'Englisch',
        de: 'Deutsch',
        theme: 'Hintergrund wählen',
        dark: 'Dunkel',
        light: 'Hell'
    },
    createQueue: {
        title: 'Schlange erstellen',
        availability: 'Maximale Verfügbarkeit *',
        duration: 'Geschätzte Dauer in Stunden *',
        name: 'Wie heisst ihre Schlange *',
        description: 'Kommentar',
        button: 'Schlange erstellen'
    },
    detailQueue: {
        title: 'Schlangeninformation',
        state: 'Status:',
        progress: 'Fortschritte:',
        timeService: 'Betriebszeit:',
        waitingTime: 'Wartezeit:',
        share: 'Schlange teilen',
        pause: 'Pausieren',
        resume: 'Wieder aufnehmen',
        finish: 'Beenden'
    },
    detailTicket: {
        title: 'Ticketinformation',
        state: 'Status:',
        progress: 'Fortschritt:',
        timeService: 'Betriebszeit:',
        waitingTime: 'Wartezeit:',
        donate: 'Ticket spenden',
        consume: 'Ticket benützen',
        abandom: 'Schlange verlassen'
    },
    documentation: {
        title: 'Information',
        header1: 'Was ist PocaCola',
        paragraph1: 'PocaCola ist eine leistungsstarke, integrierbare und skalierbare Plattform für die Verwaltung des Bedarfs und der Distribution von Waren und Dienstleistungen. Sie bietet für B2C- und B2B-Modelle Managementlösungen an.',
        header2: 'Worin liegt die Notwendigkeit von PocaCola',
        paragraph2: 'Die Nachfrage nach physischen und virtuellen Waren und Dienstleistungen wächst stetig, doch in den letzten zwei Jahren ist das Angebot aufgrund der COVID-Quarantäne und der steigenden Logistikkosten zurückgegangen, wie auch ins stocken geraten.',
        paragraph3: 'Aus diesen Gründen ist ein Mangel an Waren und Dienstleistungen auf der einen Seite und an Nachfrage bei angestauten Güter auf der anderen eine Ursache für soziale Unzufriedenheit, die zu Konflikten und einer schlechten Nutzung der Zeit durch Warteschlangen führt.',
        paragraph4: 'Was wäre, wenn wir Engpässe effizienter und mit weniger Frustration bewältigen könnten?'
    },
    joinQueue: {
        title: 'Einer Schlange beitreten',
        code: 'Schlangencode *',
        button: 'Der Schlange beitreten',
    },
    listQueue: {
        title: 'Meine Schlangen',
        progress: 'Fortschritt:',
        empty: 'Keine erstellte Schlangen',
        active: 'Aktive Schlangen',
        pause: 'Pausierte Schlangen',
        finish: 'Beenden',
    },
    listTicket: {
        title: 'Meine Tickets',
        state: 'Fortschritt:',
        empty: 'Keine Tickets gefunden',
        active: 'Aktive Tickets',
        completed: 'Abgeschlossene Tickets',
        abandoned: 'Verlassene Tickets',
        abandom: 'Verlassen'
    },
    processScan: {
        title: 'Überprüfung',
        errorQueue: 'Aktion fehlgeschlagen. Der Schlange beitreten zurzeit nicht möglich',
        successQueue: 'Aktion erfolgreich',
        errorTicket: 'Aktion fehlgeschlagen. Ticket ist ungültig',
        successTicket: 'Aktion erfolgreich',
    },
    qrCode: {
        title: 'Erstellter Code',
        labelQueue: 'Der Schlange beitreten',
        labelTicket: 'Ticket scannen',
        shareQueue: 'Code der Schlange teilen',
        shareTicket: 'Ticket senden',
    },
    qrScan: {
        title: 'Code scannen',
        codeQueue: 'Schlangencode einfügen',
        scanAgain: 'Erneut scannen'
    },
    general: {
        day: 'Tag,',
        days: 'Tage,',
        hour: 'Stunde,',
        hours: 'Stunden,',
        minutes: 'Minuten,',
        leastOneMin: 'Weniger als eine Minute',
        calculating: 'Berechnen...'
    },
    messages:{
        TICKET_NOT_EXIST: 'Ticket wird nicht gefunden',
        QUEUE_NOT_ACTIVATED: 'Schlange ist nicht aktiv',
        QUEUE_NOT_EXIST: 'Schlange wird nicht gefunden',
        QUEUE_USER_AND_MANAGER_EQUAL: 'Ersteller der Schlange kann kein Ticket erhalten',
        TICKET_EXIST: 'Dieses Ticket ist bereits vergeben',
        USER_ALREADY_HAVE_TICKET: 'El usuario ya tiene ticket',
        TICKET_EXPIRED: 'Das Ticket ist abgelaufen und funktioniert nicht.',
        TICKET_CORRUPTED: 'Das Ticket wurde von böswilligen Benutzern beschädigt.',
        TICKET_NOT_AUTHORIZED_VEFIFIER: 'Der derzeitige Überprüfer kann dieses Ticket nicht autorisieren.',
        QUEUE_FINALIZED: 'Die Schlange wurde beendet',
        CREATED_QUEUE: 'Schlange erfolgreich erstellt',
        JOINED_QUEUE: 'Erfolgreich der Schlange beigetreten',
        FINALIZED_QUEUE: 'Schlange erfolgreich abgeschlossen',
        PAUSED_QUEUE: 'Schlange pausiert',
        RESUME_QUEUE: 'Schlange wiederaufgenommen',
        GIVE_UP_TICKET: 'Ticket verlassen',
        VERIFIED_TICKET: 'Ticket wurde erfolgreich verifiziert',
        SOCKET_DISCONNECTED: 'Internetverbindung verloren',
        NOT_CONNECTION: 'Internetverbindung überprüfen. Versuchen Sie, eine Verbindung zu einem Wifi-Netzwerk herzustellen oder mobile Daten zu aktivieren.',
    }
}