import * as Localization from 'expo-localization';
//import I18n from 'i18n-js';
import I18n from 'i18n-js';

//import memoize from 'lodash.memoize'; // Use for caching/memoize for better performance
// import { I18nManager } from 'react-native';

import en from './en';
import es from './es';
import de from './de';

I18n.locale = Localization.locale;

I18n.translations = {
  default: en,
  'en-US': en,
  en,
  es,
  de
};

I18n.fallbacks = true;
export default I18n;
