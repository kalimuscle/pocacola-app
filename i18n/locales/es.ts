export default {
	homepage: {
		title: 'Inicio',
	  	welcome: 'Bienvenido a PocaCola',
	 	 button1: 'Crear cola',
	  	button2: 'Unirse a cola',
	  	invitationText: 'Invita a tus amigos a usar PocaCola',
	  	inviteButton: 'Invitar a mis amigos',
	},
	configuration: {
		title: 'Ajustes',
		language: 'Seleccionar idioma',
		es: 'Español',
		en: 'Ingles',
		de: 'Aleman',
		theme: 'Seleccionar tema',
		dark: 'Oscuro',
		light: 'Claro'
	},
	createQueue: {
		title: 'Crear cola',
		availability: 'Maxima disponibilidad *',
		duration: 'Duracion estimada en horas *',
		name: 'Como se llamara su cola *',
		description: 'Comentario',
		button: 'Crear cola'
	},
	detailQueue: {
		title: 'Informacion de la cola',
		state: 'Estado:',
		progress: 'Progreso:',
		timeService: 'Tiempo en Servicio:',
		waitingTime: 'Espera x turno:',
		share: 'Compartir cola',
		pause: 'Pausar',
		resume: 'Reanudar',
		finish: 'Terminar'
	},
	detailTicket: {
		title: 'Informacion del ticket',
		state: 'Estado:',
		progress: 'Progreso:',
		timeService: 'Tiempo en Servicio:',
		waitingTime: 'Espera x turno:',
		donate: 'Donar ticket',
		consume: 'Usar ticket',
		abandom: 'Abandonar cola'
	},
	documentation: {
		title: 'Informacion',
		header1: 'Que es PocaCola',
		paragraph1: 'PocaCola es una plataforma de gestion y organizacion para la demanda de bienes y servicios, integrable, escalable y de alto rendimiento disponible para soluciones B2C y B2B.',
		header2: 'Por que es necesario PocaCola',
		paragraph2: 'La demanda de bienes y servicios, fisicos y virtuales no para de crecer, sin embargo en los ultimos 2 años la oferta se ha contraido debido a las cuarentenas de la COVID, el encarecimiento de la logistica y la guerra ruso-ucraniana.',
		paragraph3: ' Por estas razones la escasez de bienes y servicios es una causa de descontento social que genera conflictos y un mal empleo del tiempo al tener que hacer colas.',
		paragraph4: ' Que tal si podemos gestionar la escasez de un modo mas eficiente y menos frustantes para los clientes de comercios.'
	},
	joinQueue: {
		title: 'Unir a una cola',
		code: 'Codigo de la cola *',
		button: 'Unirme a la cola',
	},
	listQueue: {
		title: 'Mis colas',
		progress: 'Progreso:',
		empty: '0 colas creadas',
		active: 'Colas activas',
		pause: 'Colas pausadas',
		finish: 'Terminar',
	},
	listTicket: {
		title: 'Mis tickets',
		state: 'Progreso:',
		empty: '0 tickets encontrados',
		active: 'Tickets activos',
		completed: 'Tickets completados',
		abandoned: 'Tickets abandonados',
		abandom: 'Abandonar'
	},
	processScan: {
		title: 'Verificacion de elemento',
		errorQueue: 'Operacion fallida. No puede unirse a la cola',
		successQueue: 'Operacion exitosa',
		errorTicket: 'Operacion fallida. Ticket no valido',
		successTicket: 'Operacion exitosa',
	},
	qrCode: {
		title: 'Codigo generado',
		labelQueue: 'Unete a la cola',
		labelTicket: 'Escanea ticket',
		shareQueue: 'Compartir codigo de cola',
		shareTicket: 'Enviar ticket de cola',
	},
	qrScan: {
		title: 'Escanear codigo',
		codeQueue: 'Insertar codigo de cola',
		scanAgain: 'Escanear otra vez'
	},
	general: {
		day: 'dia,',
		days: 'dias,',
		hour: 'hora,',
		hours: 'horas,',
		minutes: 'min',
		leastOneMin: 'menos de 1 minuto',
		calculating: 'calculando ...'
	},
	messages:{
		TICKET_NOT_EXIST: 'El ticket no existe',
		QUEUE_NOT_ACTIVATED: 'La cola no esta activa',
		QUEUE_NOT_EXIST: 'La cola no existe',
		QUEUE_USER_AND_MANAGER_EQUAL: 'El creador de la cola no puede obtener ticket',
		TICKET_EXIST: 'Este ticket ya esta ocupado',
		USER_ALREADY_HAVE_TICKET: 'El usuario ya tiene ticket',
		TICKET_EXPIRED: 'El ticket ha expirado y no es funcional',
		TICKET_CORRUPTED: 'El ticket ha sido corrompido por usuarios malintencionados',
		TICKET_NOT_AUTHORIZED_VEFIFIER: 'El verificador actual no puede autorizar este ticket',
		QUEUE_FINALIZED: 'La cola ha finalizado',

		CREATED_QUEUE: 'Cola creada con exito',
		JOINED_QUEUE: 'Se ha unido a la cola con exito',
		FINALIZED_QUEUE: 'Ha finalizado cola con exito',
		PAUSED_QUEUE: 'Cola pausada',
		RESUME_QUEUE: 'Cola reanudada',
		GIVE_UP_TICKET: 'Ticket abandonado',
		VERIFIED_TICKET: 'Ticket fue verificado con exito',
		SOCKET_DISCONNECTED: 'Conexion a internet perdida',
		NOT_CONNECTION: 'La conexion a Internet. Intente conectarse a una red Wifi o activar datos moviles',
	},
	alert: {
		title_queue: 'Finalizar cola',
		text_queue: 'Al finalizar una cola activa es altamente probable que genere malestar entre los usaurios del servicio, penalizando futuros ingresos. Esta seguro de finalizarla?',
		title_ticket: 'Salir de la cola',
		text_ticket: 'Seguro que desea salir de la cola?',
		button_cancel: 'Cancel',
		button_accept_queue: 'Si, finalizar',
		button_accept_ticket: 'Si, salir'
	}
  }