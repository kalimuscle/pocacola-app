export default {
	homepage: {
		title: 'Home',
	  	welcome: 'Welcome to PocaCola',
	 	button1: 'Create queue',
	  	button2: 'Joint to queue',
	  	invitationText: 'Invite your friends to use PocaCola',
	  	inviteButton: 'Invite my friends',
	},
	configuration: {
		title: 'Settings',
		language: 'Select language',
		es: 'Spanish',
		en: 'English',
		de: 'German',
		theme: 'Select theme',
		dark: 'Dark',
		light: 'Light'
	},
	createQueue: {
		title: 'Create queue',
		availability: 'Maximum availability *',
		duration: 'Estimated duration in hours *',
		name: 'What is his queue called? *',
		description: 'Comment',
		button: 'Create queue'
	},
	detailQueue: {
		title: 'Queue informationa',
		state: 'Condition:',
		progress: 'Progress:',
		timeService: 'Time in Service:',
		waitingTime: 'Wait for turn:',
		share: 'Share queue',
		pause: 'Pause',
		resume: 'Resume',
		finish: 'End up'
	},
	detailTicket: {
		title: 'Ticket information',
		state: 'Condition:',
		progress: 'Progress:',
		timeService: 'Time in Service:',
		waitingTime: 'Wait for turn:',
		donate: 'Donate ticket',
		consume: 'Use ticket',
		abandom: 'Leave queue'
	},
	documentation: {
		title: 'Information',
		header1: 'What is Pococola?',
		paragraph1: 'PocaCola is a management and organization platform for the demand for goods and services, integrable, scalable and high performance available for B2C and B2B solutions.',
		header2: 'Why is Pococola necessary?',
		paragraph2: 'The demand for physical and virtual goods and services does not stop growing, however in the last 2 years the supply has contracted due to the COVID quarantines, the increase in logistics costs and the Russian-Ukrainian war.',
		paragraph3: ' For these reasons, the scarcity of goods and services is a cause of social discontent that generates conflicts and a misuse of time by having to queue.',
		paragraph4: ' What if we can manage shortages more efficiently and less frustratingly for retail customers?'
	},
	joinQueue: {
		title: 'Join a queue',
		code: 'Queue code *',
		button: 'Join the queue',
	},
	listQueue: {
		title: 'My queues',
		progress: 'Progress:',
		empty: '0 queues created',
		active: 'Active queues',
		pause: 'Queues paused',
		finish: 'End up',
	},
	listTicket: {
		title: 'My tickets',
		state: 'Progress:',
		empty: '0 tickets found',
		active: 'Active tickets',
		completed: 'Completed tickets',
		abandoned: 'Abandoned tickets',
		abandom: 'Quit'
	},
	processScan: {
		title: 'Element Verification',
		errorQueue: 'Failed operation. Can not join queue',
		successQueue: 'Successful operation',
		errorTicket: 'Failed operation. Invalid ticket',
		successTicket: 'Successful operation',
	},
	qrCode: {
		title: 'Generated code',
		labelQueue: 'Join the queue',
		labelTicket: 'Scan receipt',
		shareQueue: 'Share queue code',
		shareTicket: 'Submit queue ticket',
	},
	qrScan: {
		title: 'Scan code',
		codeQueue: 'Insert queue code',
		scanAgain: 'Scan again'
	},
	general: {
		day: 'day,',
		days: 'days,',
		hour: 'hour,',
		hours: 'hours,',
		minutes: 'min',
		leastOneMin: 'less than 1 minute',
		calculating: 'calculated ...'
	},
	messages:{
		TICKET_NOT_EXIST: 'The ticket does not exist',
		QUEUE_NOT_ACTIVATED: 'The queue is not active',
		QUEUE_NOT_EXIST: 'Queue does not exist',
		QUEUE_USER_AND_MANAGER_EQUAL: 'Queue creator can not get ticket',
		TICKET_EXIST: 'This ticket is already taken',
		USER_ALREADY_HAVE_TICKET: 'The user already has a ticket',
		TICKET_EXPIRED: 'The ticket has expired and is not functional',
		TICKET_CORRUPTED: 'The ticket has been corrupted by malicious users',
		TICKET_NOT_AUTHORIZED_VEFIFIER: 'The current verifier cannot authorize this ticket',
		QUEUE_FINALIZED: 'The queue has ended',

		CREATED_QUEUE: 'Queue created successfully',
		JOINED_QUEUE: 'You have joined the queue successfully',
		FINALIZED_QUEUE: 'Queue has ended successfully',
		PAUSED_QUEUE: 'Queue paused',
		RESUME_QUEUE: 'Queue resumed',
		GIVE_UP_TICKET: 'Abandoned ticket',
		VERIFIED_TICKET: 'Ticket was successfully verified',
		SOCKET_DISCONNECTED: 'Internet connection lost',
		NOT_CONNECTION: 'The Internet connection. Try to connect to a Wi-Fi network or activate mobile data',
	},
	alert: {
		title_queue: 'Finalizar cola',
		text_queue: 'Al finalizar una cola activa es altamente probable que genere malestar entre los usaurios del servicio, penalizando futuros ingresos. Esta seguro de finalizarla?',
		title_ticket: 'Salir de la cola',
		text_ticket: 'Seguro que desea salir de la cola?',
		button_cancel: 'Cancel',
		button_accept_queue: 'Si, finalizar',
		button_accept_ticket: 'Si, salir'
	}
  }