export const SHOW_SPLASH = "SHOW_SPLASH";
export const HIDE_SPLASH = "HIDE_SPLASH";
export const REQUEST_CAMERA_PERMISSION = "REQUEST_CAMERA_PERMISSION";
export const THEME_CHANGE = 'THEME_CHANGE';
export const LANGUAGE_CHANGE = 'LANGUAGE_CHANGE';

export const SOCKET = "SOCKET";
export const GENERATE_GUEST_USER = "GENERATE_GUEST_USER";

export const ADD_QUEUE = "ADD_QUEUE";
export const REMOVE_QUEUE = "REMOVE_QUEUE";
export const PAUSE_QUEUE = "PAUSE_QUEUE";
export const RESUME_QUEUE = "RESUME_QUEUE";
export const LIST_QUEUE = "LIST_QUEUE";
export const ADD_TICKET = "ADD_TICKET";
export const REMOVE_TICKET = "REMOVE_TICKET";
export const LIST_TICKET = "LIST_TICKET";