import * as types from "./types";

const initialState = {
    lang: 'es',
    showSplash: true,
    connected: false,
    cameraPermission: null,
    theme: 'dark', //light
    tickets: [],
    queues: [],
    sub: '',
}

const genRandomSecretKeyHex = (size: number) => [...Array(size)].map(() => Math.floor(Math.random() * 16).toString(16)).join('');

const mainReducer = (state: any = initialState, action : any) => {
    switch (action.type) {
        case types.SHOW_SPLASH:
            return {
                ...state,
                showSplash: true
        }
        case types.SOCKET:
            return {
                ...state,
                socket: action.payload
        }
        case types.HIDE_SPLASH:
            return {
                ...state,
                showSplash: false
        }
        
        case types.THEME_CHANGE:
            return {
                ...state,
                theme: action.payload
        }
        case types.LANGUAGE_CHANGE:
            return {
                ...state,
                lang: action.payload
        }
        case types.REQUEST_CAMERA_PERMISSION:
            return {
                ...state,
                cameraPermission: action.payload
        }

        case types.GENERATE_GUEST_USER:
            return {
                ...state,
                sub: genRandomSecretKeyHex(20)
        }
        
        case types.ADD_QUEUE:
            //alert('ADD_QEUE');
            return {
                ...state,
                queues: [... state.queues, action.payload]
        }

        case types.ADD_TICKET:
            //alert('ADD_QEUE');
            return {
                ...state,
                tickets: [... state.tickets, action.payload]
        }

        case types.LIST_QUEUE:
            return {
                ...state,
                queues: action.payload,
        }

        case types.REMOVE_QUEUE:
            return {
                ...state,
                queues: state.queues.filter((item: any) => item.id != action.payload),
        }

        case types.LIST_TICKET:
            return {
                ...state,
                tickets: action.payload,
        }

        case types.REMOVE_TICKET:
            return {
                ...state,
                tickets: state.tickets.filter((item: any) => item.id != action.payload),
        }

        // case types.RESUME_QUEUE:
        //     return {
        //         ...state,
        //         qeues: state.qeues.filter((item: any) => item.id != action.payload),
        //        // qeues: state.qeues.forEach((item) => { if(item.id == action.payload) return item.state = 'resume'}),
        // }
        // case types.PAUSE_QUEUE:
        //     return {
        //         ...state,
        //         qeues: state.qeues.filter((item: any) => item.id != action.payload),
        // }
        default:
        return state;
    }
}
export default mainReducer