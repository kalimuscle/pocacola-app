import { Socket } from "socket.io-client"
import * as actions from "./types"

export const showSplash = () => ({
    type: actions.SHOW_SPLASH
})

export const themeUpdate = (theme: string) => ({
    type: actions.THEME_CHANGE,
    payload: theme
})

export const languageChange = (lang: string) => ({
    type: actions.LANGUAGE_CHANGE,
    payload: lang
})

export const defineSocket = (socket: Socket) => ({
    type: actions.SOCKET,
    payload: socket
})

export const hideSplash = () => ({
    type: actions.HIDE_SPLASH
})

export const requestCameraPermission = ( requestState: Boolean) => ({
    type: actions.REQUEST_CAMERA_PERMISSION,
    payload: requestState
})

export const generateGuestUser = () => ({
    type: actions.GENERATE_GUEST_USER,
})

export const addNewQueue = (queue) => ({
    type: actions.ADD_QUEUE,
    payload: queue
})

export const removeQueue = (id) => ({
    type: actions.REMOVE_QUEUE,
    payload: id
})

export const pauseQueue = (id) => ({
    type: actions.PAUSE_QUEUE,
    payload: id
})

export const listQueue = (queues) => ({
    type: actions.LIST_QUEUE,
    payload: queues
})

export const resumeQueue = (id) => ({
    type: actions.RESUME_QUEUE,
    payload: id
})

export const addTicket = (ticket) => ({
    type: actions.ADD_TICKET,
    payload: ticket
})



export const removeTicket = (id) => ({
    type: actions.REMOVE_TICKET,
    payload: id
})

export const listTicket = (tickets) => ({
    type: actions.LIST_TICKET,
    payload: tickets
})